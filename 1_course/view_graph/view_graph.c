#include "graph.h"

int check_flags(int* flags, int checked, int length)
{
	int i;
	if(checked==-2)
	{
		return 0;
	}
	for(i=0;i<length;i++)
	{
		if(flags[i]==checked)
		{
			return 0;
		}
	}
	return 1;
}

int* a_breadth_first_search(graph_user graph)			//Обход графа в ширину
{
	int length=give_length(graph);
	int* flags = calloc(length,sizeof(int));
	int i=1,j=0,k=0;
	flags[0]=0;
	while(j<length)
	{
		for(k=0;k<length;k++)
		{
			if(check_arc(graph,j,k)==1)
			{
				if(check_flags(flags,k,i)!=0)
				{
					flags[i]=k;
					i++;
				}
			}
		}
		j++;
	}
	return flags;
}

int* go_arcs_depth(graph_user graph, int start,char* flags,int* counter,int* rezult,int length)
{
	rezult[*counter]=start;
	*counter=*counter+1;
	flags[start]=1;
	int j=0;
	for(j=0;j<length;j++)
	{
		if(check_arc(graph,start,j)==1)
		{
			if(flags[j]!=1)
			{
				go_arcs_depth(graph,j, flags,counter,rezult,length);
			}
		}
	}
	return rezult;
}

int* the_depth(graph_user graph,int start)		//обход графа в глубину
{
	int length=give_length(graph);
	if(start>length||start<0)
	{
		return NULL;
	}
	int counter=0;
	char* flags = calloc(length, sizeof(char));
	int* road=calloc(length,sizeof(int));
	flags[start]=1;
	road=go_arcs_depth(graph,start,flags,&counter,road,length);
	free(flags);
	return road;
}

int main(int argc, char* argv[])
{
	if(argc!=2)
	{
		puts("Error run!");
		return 0;
	}
	graph_user graph;
	graph=load_graph(argv[1],1);
	int* road;
	road = a_breadth_first_search(graph);
	int i=0;
	for(i=0;i<give_length(graph);i++)
	{
		printf("%d ", road[i]+1);
	}
	printf("\n");
	road = the_depth(graph,0);
	for(i=0;i<give_length(graph);i++)
	{
		printf("%d ", road[i]+1);
	}
	delete_graph(graph);
	free(road);
}