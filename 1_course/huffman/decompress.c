#include "decompress.h"

typedef struct tr
{
	char smb;
	int branch;
	struct tr* parent;
	struct tr* zero;
	struct tr* one;
}Tree;

typedef struct mp
{
	char smb;
	int freq;
	Tree* node;
}map;

int decompress(char* addres)
{
	FILE* input = fopen(addres,"rb");
	int length;
	if(fread(&length,4,1,input)==0)
	{
		puts("incorrect input file");
		return;
	}
	int i;
	char* out_addres=calloc(length,sizeof(char));
	for(i=0;i<length;i++)
	{
		if(fread((out_addres+i),1,1,input)==0)
		{
			puts("incorrect input file");
			return;
		}
	}
	if(fread(&length,4,1,input)==0)
	{
		puts("incorrect input file");
		return;
	}
	if(length==0)
	{
		FILE* output=fopen(out_addres,"wb");
		return;
	}
	map* map_freq=calloc(length,sizeof(map));
	for (i=0;i<length;i++)
	{
		char tmp1=0;
		int tmp2=0;
		if(fread(&tmp1,1,1,input)==0)
		{
			puts("incorrect input file");
			return;
		}
		map_freq[i].smb=tmp1;
		if(fread(&tmp2,4,1,input)==0)
		{
			puts("incorrect input file");
			return;
		}
		map_freq[i].freq=tmp2;
		map_freq[i].node = calloc(1,sizeof(Tree));
		map_freq[i].node->smb=tmp1;
		map_freq[i].node->parent=NULL;
		map_freq[i].node->one=NULL;
		map_freq[i].node->zero=NULL;
	}
	Tree* Node = make_tree(map_freq,length);
	Tree* node_run=Node;
	FILE* output=fopen(out_addres,"wb");
	while(!feof(input))
	{
		char smb;
		int branch;
		if(fread(&smb,1,1,input)==0)
		{
			if(!feof(input))
			{
				puts("incorrect input file");
				return;
			}
		}
		if(!feof(input))
		{
			for(i=7;i>-1;i--)
			{
				branch = ((smb & (1 << i)) != 0);
				if(node_run->one==NULL&&node_run->zero==NULL)
				{
					if(node_run->parent==NULL)
					{
						if(i>0)
						{
							branch = ((smb & (1 << (i-1))) != 0);
						}
						if(branch)
						{
							fwrite(&(node_run->smb),1,1,output);
							node_run=Node;
							continue;
						}
						else
						{
							return;
						}
					}
					if(branch)
					{
						fwrite(&(node_run->smb),1,1,output);
						node_run=Node;
						continue;
					}
				}
				if(branch==1)
				{
					node_run=node_run->one;
				}
				else
				{
					node_run=node_run->zero;
				}
			}
		}
	}
	free(map_freq);
	free(Node);
	fclose(input);
	fclose(output);
}