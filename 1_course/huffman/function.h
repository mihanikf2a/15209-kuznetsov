#ifndef FUNCTION_H
#define FUNCTION_H

#include <stdlib.h>

void* sort(void* mass,int* lenght);
void swap_i(int* a, int* b);
void swap_c(char* a,char* b);
void* make_tree(void* v_tree_map, int length);

#endif