#include "function.h"

typedef struct tr
{
	char smb;
	int branch;
	struct tr* parent;
	struct tr* zero;
	struct tr* one;
}Tree;

typedef struct mp
{
	char smb;
	int freq;
	Tree* node;
}map;

void swap_i(int* a, int* b)
{
	int c=*a;
	*a=*b;
	*b=c;
}
void swap_c(char* a,char* b)
{
	char c=*a;
	*a=*b;
	*b=c;
}

void add_freq(map* map_freq, int new_freq, int length, Tree* node)
{
	int i=0;
	while(map_freq[i].freq!=-1&&i<length)
	{
		i++;
	}
	map_freq[i].freq=new_freq;
	map_freq[i].node=node;
	int k,j;
	for(j=0;j<i+1;j++)
	{
		for(k=0;k<i;k++)
		{
			if(map_freq[k].freq>map_freq[k+1].freq)
			{
				swap_i(&(map_freq[k].freq),&(map_freq[k+1].freq));
				void* tmp = map_freq[k].node;
				map_freq[k].node=map_freq[k+1].node;
				map_freq[k+1].node=tmp;
			}
		}
	}
}

void* make_tree(void* v_tree_map, int length)
{
	map* tree_map=(map*)v_tree_map;
	Tree* Node=calloc((length*2)-1,sizeof(Tree));
	map* map_freq=calloc((length*2)-1,sizeof(map));
	int i=0;
	for(i=0;i<length;i++)
	{
		map_freq[i].freq=tree_map[i].freq;
		map_freq[i].node=tree_map[i].node;
	}
	for(i;i<(length*2)-1;i++)
	{
		map_freq[i].freq=-1;
		map_freq[i].node=NULL;
	}
	for(i=0;i<(length*2)-2;i+=2)
	{
		Tree* temp=calloc(1,sizeof(Tree));
		temp->zero=(map_freq+i)->node;
		temp->one=(map_freq+i+1)->node;
		temp->parent=NULL;
		map_freq[i].node->branch=0;
		map_freq[i+1].node->branch=1;
		map_freq[i].node->parent=temp;
		map_freq[i+1].node->parent=temp;
		add_freq(map_freq,map_freq[i].freq+map_freq[i+1].freq,(length*2)-1,temp);
	}
	Tree* Node2=(map_freq+(length*2)-2)->node;
	free(map_freq);
	return Node2;
}

void* sort(void* v_mass,int* length)
{
	map* mass=(map*)v_mass;
	int i=0,j;
	for(j=0;j<256;j++)
	{
		for(i=1;i<256;i++)
		{
			if(mass[i].freq>mass[i-1].freq)
			{
				swap_i(&mass[i].freq,&mass[i-1].freq);
				swap_c(&mass[i].smb,&mass[i-1].smb);
			}
		}
	}
	i=0;
	while(i<256&&mass[i].freq!=0)
	{
		i++;
	}
	*length=i;
	map* mass2=calloc(*length,sizeof(map));
	for(i=0;i<*length;i++)
	{
		mass2[i].freq=mass[*length-1-i].freq;
		mass2[i].smb=mass[*length-1-i].smb;
	}
	//free(mass);
	return mass2;
}