#include "huffman.h"

int comparesit(char* adress1, char* adress2)
{
	FILE* file1=fopen(adress1,"rb");
	FILE* file2=fopen(adress2,"rb");
	char smb1,smb2;
	while(!feof(file2)&&!feof(file1))
	{
		fread(&smb1,1,1,file1);
		fread(&smb2,1,1,file2);
		if(smb2!=smb1)
		{
			puts("files differ");
			fclose(file2);
			fclose(file1);
			return 1;
		}
	}
	puts("the files are the same");
	fclose(file2);
	fclose(file1);
	return 0;
}
int main(int argc, char** argv)
{
	if(argc!=3&&argc!=4)
	{
		puts("run error!");
		return 1;
	}
	if(argc==4)
	{
		if(*argv[1]=='s')
		{
			printf("%d",comparesit(argv[2],argv[3]));
		}
		else
		{
			puts("incorrect mode selection");
		}
	}
	else
	{
		switch(*argv[1])
		{
			case 'c'	: compress(argv[2]); break;
			case 'd'	: decompress(argv[2]); break;
			default		: puts("incorrect mode selection!"); return 1;
		}
	}
	return 0;
}