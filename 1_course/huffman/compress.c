#include "compress.h"

typedef struct tr
{
	char smb;
	int branch;
	struct tr* parent;
	struct tr* zero;
	struct tr* one;
}Tree;

typedef struct mp
{
	char smb;
	int freq;
	Tree* node;
}map;

int* make_key(Tree* node, map* tree_map,int length,int smb,int* len_key)
{
	int i=0;
	int* key=calloc(length+1,sizeof(int));
	while(tree_map[i].smb!=(char)smb)
	{
		i++;
	}
	Tree* Node=tree_map[i].node;
	key[0]=1;
	i=1;
	if(Node->parent==NULL&&Node->zero==NULL&&Node->one==NULL)
	{
		printf("%d",Node->branch );
		key[i]=Node->branch;
		i++;
	}
	while(Node->parent!=NULL)
	{
		key[i]=Node->branch;
		Node=Node->parent;
		i++;
	}
	*len_key=i;
	return key;
}
void print_map(FILE* output, map* map_freq, int length)
{
	fwrite(&length,4,1,output);
	int i;
	for(i=0;i<length;i++)
	{
		fwrite(&((map_freq+i)->smb),1,1,output);
		fwrite(&((map_freq+i)->freq),4,1,output);
	}
}

void print_file(int* key,int*key_end,FILE* output, int len_key)
{
	int i=0,k=0,j=0;
	for(i=0;i<len_key/2;i++)
	{
		swap_i(key+i,key+len_key-i-1);
	}
	printf("\n");
	char smb;
	int len_key_end=0;
	if(*key_end!=-1)
	{
		i=0;
		while(key_end[i]!=-1)
		{
			i++;
		}
		len_key_end=i;
		while(i<8&&k<len_key)
		{
			key_end[i]=key[k];
			k++;
			i++;
			len_key_end++;
		}
		if(len_key_end<8)
		{
			return;
		}
		smb = 0;
		for (j = 0; j < 8; j++)
		{
			if (*(key_end + j) == 1)
			{
				smb |= ( 1 << (7-j) );
			}
		}
		fwrite(&smb, 1, 1, output);
	}
	int* key_part=key+k;
	int len_key_part=len_key-k;
	for(i=0;i<len_key_part/8;i++)
	{
		smb = 0;
		for (j = 0; j < 8; j++)
		{
			if (*(key_part + (i*8) + j) == 1)
			{
				smb |= ( 1 << (7-j) );
			}
		}
		fwrite(&smb, 1, 1, output);
	}
	for(i=0;i<8;i++)
	{
		key_end[i]=-1;
	}
	if(len_key_part%8>0)
	{
		for(i=(len_key_part - len_key_part%8),j=0;i<len_key_part;i++,j++ )
		{
			*(key_end+j)=key_part[i];
		}
	}
}

void print_name(FILE* output,char* addres)
{
	int i,j;
	int len=strlen(addres);
	for(i=len-1;i>-1;i--)
	{
		if(addres[i]=='\\')
		{
			break;
		}
	}
	if(i==-1)
	{
		i=0;
	}
	int ln=len-i;
	fwrite(&ln,4,1,output);
	if(i==-1)
	{
		i=0;
	}
	for(j=i;j<len;j++)
	{
		fwrite((addres+j),1,1,output);
	}
}

void print_key_end(int *key_end, FILE* output)
{
	int j;
	char smb;
	for (j = 0; j < 8; j++)
	{
		if (*(key_end + j) == 1)
		{
			smb |= ( 1 << (7-j) );
		}
	}
	fwrite(&smb,1,1,output);
	free(key_end);
}
char* make_out_name(char* addres)
{
	int i,j;
	int len=strlen(addres);
	for(i=len-1;i>-1;i--)
	{
		if(addres[i]=='.')
		{
			break;
		}
	}
	len=i;
	for(i=len-1;i>-1;i--)
	{
		if(addres[i]=='\\')
		{
			break;
		}
	}
	if(i==-1)
	{
		i=0;
	}
	len-=i;
	char* out_name=calloc(len+4,sizeof(char));
	int k=0;
	for(j=i;j<len+i;j++)
	{
		out_name[k]=addres[j];
		k++;
	}
	strcat(out_name,".huf");
	return out_name;
}

int compress(char* addres)
{
	map* tree_map=calloc(256,sizeof(map));
	int i;
	for(i=0;i<256;i++)
	{
		tree_map[i].smb=(char)i;
		tree_map[i].freq=0;
	}
	FILE* input=fopen(addres,"rb");
	int tmp;
	while(!feof(input))
	{
		tmp=fgetc(input);
		tree_map[tmp].freq++;
	}
	fclose(input);
	int length=256;
	tree_map=(map*)sort(tree_map,&length);
	for(i=0;i<length;i++)
	{
		tree_map[i].node=calloc(1,sizeof(Tree));
		tree_map[i].node->smb=tree_map[i].smb;
		tree_map[i].node->parent=NULL;
		tree_map[i].node->one=NULL;
		tree_map[i].node->zero=NULL;
	}
	if(length==0)
	{
		char* out_name;
		out_name=make_out_name(addres);
		FILE* output=fopen(out_name,"wb");
		print_name(output,addres);
		print_map(output,tree_map,length);
		fclose(output);
		return;
	}
	Tree* node;
	node=make_tree(tree_map, length);
	input=fopen(addres,"rb");
	char* out_name;
	out_name=make_out_name(addres);
	FILE* output=fopen(out_name,"wb");
	print_name(output,addres);
	print_map(output,tree_map,length);
	int len_key=0;
	int* key_end=calloc(8,sizeof(int));
	for(i=0;i<8;i++)
	{
		key_end[i]=-1;
	}
	while(!feof(input))
	{
		tmp=fgetc(input);
		if(!feof(input))
		{
			int* key=make_key(node,tree_map,length,tmp,&len_key);
			print_file(key,key_end,output,len_key);
			free(key);
			len_key=0;
		}
	}
	print_key_end(key_end, output);
	fclose(input);
	fclose(output);
}