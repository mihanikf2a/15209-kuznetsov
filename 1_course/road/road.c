#include "graph.h"

int min(int* mass,int length,int* flags)
{
	int min_rez=-1;
	int i=0;
	for(i=0;i<length;i++)
	{
		if(flags[i]!=1&&mass[i]!=-1)
		{
			min_rez=i;
			break;
		}
	}
	if(min_rez==-1)
	{
		return -1;
	}
	for(i=0;i<length;i++)
	{
		if(mass[i]>0 && mass[i]<mass[min_rez]&&flags[i]!=1)
		{
			min_rez=i;
		}
	}
	return min_rez;
}

int* dijkstra(graph_user graph,int start)			//Обход графа в ширину
{
	int length=give_length(graph);
	if(start>length||start<0)
	{
		return NULL;
	}
	int* road=calloc(length,sizeof(int));
	int i=0,j=0;
	for(i=0;i<length;i++)
	{
		road[i]=-1;
	}
	int* flags=calloc(length,sizeof(i));
	flags[start]=1;
	road[start]=0;
	for(j=0;j<length;j++)
	{
		for(i=0;i<length;i++)
		{
			if(check_arc(graph,start,i)==1)
			{
				if(road[i]!=-1)
				{
					if((road[start]+give_param(graph,start,i))<=road[i])
					{
						road[i]=road[start]+give_param(graph,start,i);
					}
				}
				else
				{
					road[i]=road[start]+give_param(graph,start,i);
				}
			}
		}
		flags[start]=1;
		if(min(road,length,flags)==-1)
		{
			return road;
		}
		start=min(road,length,flags);
	}
	return road;
}

int main(int argc, char* argv[])
{
	if(argc==1||argc>2)
	{
		puts("Error run!");
		return 0;
	}
	graph_user graph;
	graph=load_graph(argv[1],2);
	int start,end;
	int* road;
	while(1)
	{
		scanf("%d",&start);
		if(start>0)
		{
			scanf("%d",&end);
			if(end>0)
			{
				road = dijkstra(graph,start-1);
				if(road[end-1]!=-1)
				{
					printf("\n%d\n", road[end-1]);
				}
				else
				{
					puts("no road");
				}
			}
			else
			{
				delete_graph(graph);
				if(road!=NULL)
				{
					free(road);
				}
				return 0;
			}
		}
		else
		{
			delete_graph(graph);
			if(road!=NULL)
			{
				free(road);
			}
			return 0;
		}
	}
}