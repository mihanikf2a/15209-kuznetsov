#include "graph.h"

int* go_arcs_depth(graph_user graph, int start,int end,char* flags,int* counter,int* rezult,int length)
{
	rezult[*counter]=start;
	*counter=*counter+1;
	flags[start]=1;
	int j=0;
	for(j=0;j<length;j++)
	{
		if(rezult[*counter]!=end)
		{
			if(check_arc(graph,start,j)==1)
			{
				if(j==end)
				{
					rezult[*counter]=j;
					return rezult;
				}
				if(flags[j]!=1)
				{
					go_arcs_depth(graph,j,end, flags,counter,rezult,length);
				}
			}
		}
		else
		{
			return rezult;
		}
	}
	if(rezult[*counter]!=end)
	{
		*counter-=1;
		rezult[*counter]=-1;
	}
	return rezult;
}

int* the_depth(graph_user graph,int start,int end)		//обход графа в глубину
{
	int length=give_length(graph);
	if(start>length||start<0)
	{
		return NULL;
	}
	int counter=0;
	char* flags = calloc(length, sizeof(char));
	int* road=calloc(length+1,sizeof(int));
	int i=0;
	for(i=0;i<length+1;i++)
	{
		road[i]=-1;
	}
	flags[start]=1;
	road=go_arcs_depth(graph,start,end,flags,&counter,road,length);
	free(flags);
	return road;
}

int min_mass(int* mass, int length)
{
	int min=*mass;
	int i;
	for(i=0;i<length;i++)
	{
		if(mass[i]<min)
		{
			min=mass[i];
		}
	}
	return min;
}

int main(int argc, char** argv)
{
	if(argc!=2)
	{
		puts("Error run!");
		return 0;
	}
	graph_user graph1;
	graph1=load_graph(argv[1],1);
	int length=give_length(graph1);
	int start,end,i,j,min,rezult=0;
	int* mass = calloc(length,sizeof(int));
	int* road;
	while(1)
	{
		rezult=0;
		scanf("%d",&start);
		if(start==0)
		{
			break;
		}
		scanf("%d",&end);
		if(end==0)
		{
			break;
		}
		if(start>0&&start<=length&&end>0&&end<=length)
		{
			graph_user graph=copy_graph(graph1);
			road = the_depth(graph,start-1,end-1);
			while(*road!=-1)
			{
				i=1;
				j=0;
				while(road[i]!=-1&&i<length)
				{
					mass[j]=give_param(graph,road[i-1],road[i]);
					j++;
					i++;
				}
				min=min_mass(mass,j);
				int tmp;
				i=0;
				while(road[i]!=-1&&i<length)
				{
					tmp=give_param(graph,road[i-1],road[i]);
					delete_arc(graph,road[i-1],road[i]);
					if(tmp-min>0)
					{
						add_arc(graph,road[i-1],road[i],tmp-min);
					}
					i++;
					tmp=check_arc(graph, road[i],road[i-1]);
					if(tmp!=0)
					{
						tmp=give_param(graph,road[i],road[i-1]);
						delete_arc(graph,road[i],road[i-1]);
					}
					add_arc(graph,road[i],road[i-1],tmp+min);
				}
				rezult+=min;
				road = the_depth(graph,start-1,end-1);
				for (i = 0; i < length; i++)
				{
					mass[i]=0;
				}
			}
			printf("%d\n", rezult);
			delete_graph(graph);
		}
	}
	delete_graph(graph1);
	free(mass);
	free(road);
}