#include <stdlib.h>
#include <stdio.h>
#include <windows.h>

HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE) ;

int* give_size(char* addr)
{
	FILE* input = fopen(addr, "r");
	char smb = (char)fgetc(input);
	int* size=(int*)calloc(2,sizeof(int));
	int tmp_size;
	while(!feof(input))
	{
		tmp_size=0;
		while(smb!='\n'&&!feof(input))
		{
			tmp_size++;
			if(size[0]==0)
			{
				size[1]++;
			}
			if(smb!=' '&&smb!='A'&&smb!='#'&&smb!='B')
			{
				size[0]=-1;
				return size;				
			}
			smb = (char)fgetc(input);
		}
		smb = (char)fgetc(input);
		if(tmp_size!=size[1])
		{
			size[0]=-1;
			return size;
		}
		if(!feof(input))
		{
			size[0]++;
		}
	}
	size[0]++;
	fclose(input);
	return size;
}

int give_back(char** field, int* position)
{
	return ((int)(field[position[0]][position[1]])-100)*(-1);
}

int give_to(char** field, int* position,char smb,int* size)
{
	int a[4]={-10,-1,1,10};
	int i=0;
	for(i=0;i<4;i++)
	{
		if((position[0]+(a[i]/10)<size[0])&&(position[1]+(a[i]%10)<size[1])&&(position[0]+(a[i]/10)>=0)&&(position[1]+(a[i]%10)>=0))
		{
			if(field[position[0]+(a[i]/10)][position[1]+(a[i]%10)]==smb)
			{
				return a[i];
			}
		}
	}
	return 0;
}

int* go(char** field,int* position,int to,int smb)
{
	position[0]=position[0]+(to/10);
	position[1]=position[1]+(to%10);
	if(field[position[0]][position[1]]!='B')
	{	
		if(smb!=0)
		{
			field[position[0]][position[1]]=(char)(100+smb);
		}
		else
		{
			field[position[0]-(to/10)][position[1]-(to%10)]=(char)(100+smb);
		}
	}
	return position;
}

int algoritm(char** field, int* position,int* size)
{
	int to=0;
	while(field[position[0]][position[1]]!='B')
	{
		to=give_to(field,position,'B',size);
		if(to==0)
		{
			to=give_to(field,position,' ',size);
		}
		if(to!=0)
		{
			position=go(field,position,to,to);
		}
		else
		{
			to=give_back(field,position);
			if(to!=0)
			{
				position=go(field,position,to,0);
			}
			else
			{
				return 0;
			}
		}
	}
	return 1;
}

int main(int argc, char** argv)
{
	if(argc!=2)
	{
		puts("Run error!");
		return 1;
	}
	int i=0,j=0;
	int position[2]={-1,-1};
	int* size;
	size=give_size(argv[1]);
	if(size[0]==-1)
	{
		puts("bad input file!");
		return 0;
	}
	char** field=(char**)calloc(size[0],sizeof(char*));
	for(i=0;i<size[0];i++)
	{
		*(field+i)=(char*)calloc(size[1],sizeof(char));
	}
	FILE* input = fopen(argv[1], "r");
	for(i=0;i<size[0];i++)
	{
		for(j=0;j<size[1];j++)
		{
			field[i][j]=(char)fgetc(input);
			if(field[i][j]=='A')
			{
				position[0]=i;
				position[1]=j;
			}
		}
		fgetc(input);
	}
	fclose(input);
	int rez=0;
	if(position[0]!=-1&&position[1]!=-1)
	{
		rez=algoritm(field, position, size);
	}
	if(rez==0)
	{
		printf("no exit!\n");
	}
	else
	{
		for(i=0;i<size[0];i++)
		{
			for(j=0;j<size[1];j++)
			{
				if(field[i][j]>=(char)90&&field[i][j]<=(char)110)
				{
					if(field[i][j]!=100)
					{
						SetConsoleTextAttribute(hStdOut, BACKGROUND_GREEN|BACKGROUND_INTENSITY);
						printf("  ");
						SetConsoleTextAttribute(hStdOut, 0x07);
					}
					else
					{
						SetConsoleTextAttribute(hStdOut, 0x77);
						printf("  ");
						SetConsoleTextAttribute(hStdOut, 0x07);
					}
				}
				else
				{
					if(field[i][j]=='A')
					{
						SetConsoleTextAttribute(hStdOut, BACKGROUND_RED|BACKGROUND_INTENSITY);
						printf("  ");
						SetConsoleTextAttribute(hStdOut, 0x07);
					}
					if(field[i][j]=='B')
					{
						SetConsoleTextAttribute(hStdOut, 0xe7|BACKGROUND_INTENSITY);
						printf("  ");
						SetConsoleTextAttribute(hStdOut, 0x07);
					}
					if(field[i][j]=='#')
					{
						SetConsoleTextAttribute(hStdOut, 0x87|BACKGROUND_INTENSITY);
						printf("  ");
						SetConsoleTextAttribute(hStdOut, 0x07);
					}
					//printf("%c", field[i][j]);
					if(field[i][j]==' ')
					{
						SetConsoleTextAttribute(hStdOut, 0x77);
						printf("  ");
						SetConsoleTextAttribute(hStdOut, 0x07);
					}

				}
			}
			printf("\n");
		}
	}
	return 0;
}