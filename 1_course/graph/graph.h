#ifndef GRAPH_H
#define GRAPH_H

#include <stdlib.h>
#include <stdio.h>

typedef void* graph_user;

graph_user make_graph(char type,int length, char orient,char weight);
void add_arc(graph_user graph, int start, int end, int size);
void delete_arc(graph_user graph, int start, int end);
int check_arc(graph_user graph, int start, int end);
int give_param(graph_user graph, int start, int end);
graph_user copy_graph(graph_user graph);
graph_user delete_graph(graph_user graph);
graph_user load_graph(char* file_addr, char type);
void save_graph(char* file_addr, graph_user graph);
int give_length(graph_user graph_usr);

#endif