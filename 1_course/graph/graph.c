#include "graph.h"

typedef struct list
{
	void* addres;
	struct list* next;
}safe_lst;

typedef struct var_list
{
	char type;
	char orient;
	char weight;
	int length;
	void* graph_point;
} graph_user_var;

typedef struct list1
{
	int to;
	int size;
	struct  list1* next;
}graph_list;

safe_lst* safety_add(void* addres, safe_lst* safe_list)
{
	if(safe_list==NULL)
	{
		safe_list=calloc(1,sizeof(safe_lst));
		safe_list->addres=addres;
		return safe_list;
	}
	safe_lst* temp=safe_list;
	while(temp->next!=NULL)
	{
		temp=temp->next;
	}
	temp->next=calloc(1,sizeof(safe_list));
	temp=temp->next;
	temp->addres=addres;
	return safe_list;
}

int safety_check(void* addres,safe_lst* safe_list)
{
	if(addres==NULL)
	{
		return 0;
	}
	safe_lst* temp=safe_list;
	while(temp->next!=NULL)
	{
		if(temp->addres==addres)
		{
			return 1;
		}
		temp=temp->next;
	}
	if(temp->addres==addres)
	{
		return 1;
	}
	return 0;
}

int safety(void* addres,int type)
{
	static safe_lst* safe_list;
	safe_lst* temp=safe_list;
	safe_lst* temp2=temp;
	int i=0;
	switch(type)
	{
		case 0:	return safety_check(addres,safe_list); break;
		case 1:	safe_list = safety_add(addres,safe_list); break;
		case 2:	while(temp->next!=NULL)
				{
					if(temp->addres==addres)
					{
						if(i!=0)
						{
							temp2->next=temp->next;
							free(temp);
							return 0;
						}
						else
						{
							safe_list=temp->next;
							free(temp);
							return 0;
						}
					}
					temp2=temp;
					temp=temp->next;
					i++;
				}
				if(temp->addres==addres)
				{
					temp2->next=temp->next;
					free(temp);
					return 0;
				}
				break;
		default : return 0;
	}
	return 0;
}

int** make_graph_matrix(int length)		//make graph matrix. Returned point to first element of praph If length <= 0 reterned NULL
{
	int i=0,j=0;
	int** graph=calloc(length,sizeof(*graph));
	for (i = 0; i < length; i++)
	{
		*(graph+i)=calloc(length,sizeof(int));
	}
	return graph;
}

graph_list* make_graph_list(int length)		//make graph list. Returned point to first element of graph. If length <= 0 reterned NULL
{
	graph_list* graph;
	int i=0;
	graph = calloc(length,sizeof(graph_list));
	for(i=0;i<length;i++)
	{
		graph[i].next=calloc(1,sizeof(graph_list));
		graph[i].to=-2;
		graph[i].next->to=-1;
	}
	return graph;
}

graph_user make_graph(char type,int length, char orient,char weight)		//make graph. type == 1 list; type == 2 matrix
{
	if((orient!=0&&orient!=1)||(weight!=0&&weight!=1)||(type!=1&&type!=2))
	{
		return NULL;
	}
	if(length>0)
	{
		graph_user_var* graph=calloc(1,sizeof(graph_user_var));
		safety(graph,1);
		graph->length=length;
		graph->type=type;
		graph->orient=orient;
		graph->weight=weight;
		if(type==(char)1)
		{
			graph->graph_point = (graph_list*)make_graph_list(length);
			graph_user rez=(graph_user)graph;
			return rez;
		}
		else
		{
			if(type==(char)2)
			{
				graph->graph_point = (int**)make_graph_matrix(length);
				graph_user rez=(graph_user)graph;
				return rez;
			}
		}
	}
	else
	{
		return NULL;
	}
}

void add_arc_list(graph_list* graph,int start, int end,int size)		//add arcs to the grap[i] "current" - point to first arc of graph[i];
{															// "num" - the number of end arc; "size" - the weight of arc
	graph_list* arc_tmp=graph+start;
	while(arc_tmp->to!=-1)
	{
		arc_tmp=arc_tmp->next;
	}
	arc_tmp->to=end;
	arc_tmp->size=size;
	arc_tmp->next=calloc(1,sizeof(graph_list));
	arc_tmp=arc_tmp->next;
	arc_tmp->to=-1;
}

void add_arc_matrix(int** graph, int start, int end, int size) //add arc in matrix
{
	graph[start][end]=size;
}

void add_arc(graph_user graph_usr,int start, int end, int size)
{
	if(safety(graph_usr,0)==1)
	{
		if(check_arc(graph_usr,start,end)!=0)
		{
			return;
		}
		graph_user_var* graph=(graph_user_var*)graph_usr;
		if((start<0||start>=graph->length)||(end<0||end>=graph->length))
		{
			return;
		}
		if(graph->type==1)	//list
		{
			add_arc_list(graph->graph_point,start,end,size);
			if(graph->orient==0)
			{
				add_arc_list(graph->graph_point,end,start,size);
			}
			return;
		}
		if(graph->type==2)	//matrix
		{
			add_arc_matrix(graph->graph_point,start,end,size);
			if(graph->orient==0)
			{
				add_arc_matrix(graph->graph_point,end,start,size);
			}
			return;
		}
	}
}

void delete_arc_list(graph_list* current,int start,int end)		//removes an arc from the end vertex num
{
	graph_list* arc_tmp=current+start;
	graph_list* arc_tmp1=current+start;
	graph_list* rez=current;
	int flg=0;
	while(arc_tmp->to!=-1&&arc_tmp->next!=NULL)
	{
		if(arc_tmp->to==end)
		{
			arc_tmp1->next=arc_tmp->next;
			free(arc_tmp);
		}
		arc_tmp1=arc_tmp;
		arc_tmp=arc_tmp->next;
	}
	if(flg==1)
	{
		free(arc_tmp);
	}
}

void delete_arc_matrix(int** current,int start,int end)		//removes an arc from the end vertex num
{
	current[start][end]=0;
}

void delete_arc(graph_user graph_usr,int start, int end)
{
	if(safety(graph_usr,0)==1)
	{
		graph_user_var* graph=(graph_user_var*)graph_usr;
		if((start<0||start>=graph->length)||(end<0||end>=graph->length))
		{
			return;
		}
		if(graph->type==1)		//list
		{
			delete_arc_list((graph_list*)(graph->graph_point),start,end);
			if(graph->orient==0)
			{
				delete_arc_list((graph_list*)(graph->graph_point),end,start);
			}
		}
		if(graph->type==2)		//matrix
		{
			delete_arc_matrix((int**)graph->graph_point,start,end);
			if(graph->orient==0)
			{
				delete_arc_matrix((int**)graph->graph_point,end,start);
			}
		}
	}
}

int check_arc_list(graph_list* graph,int start,int end)		//checked arcs of graph list.
{
	graph_list* arc_tmp=graph+start;
	while(arc_tmp->next!=NULL&&arc_tmp->to!=-1)
	{
		if(arc_tmp->to!=-2)
		{
			if(arc_tmp->to==end)
			{
				return 1;
			}
		}
		arc_tmp=arc_tmp->next;
	}
	return 0;
}

int check_arc_matrix(int** graph,int start, int end)		//checked arcs of graph matrix.
{
	if(graph[start][end]!=0)
	{
		return 1;
	}
	return 0;
}

int check_arc(graph_user graph_usr, int start, int end)
{
	if(safety(graph_usr,0)==1)
	{
		graph_user_var* graph=(graph_user_var*)graph_usr;
		if((start<0||start>=graph->length)||(end<0||end>=graph->length))
		{
			return 0;
		}
		if(graph->type==1)		//list
		{
			return check_arc_list((graph_list*)(graph->graph_point),start,end);
		}
		if(graph->type==2)		//matrix
		{
			return check_arc_matrix((int**)graph->graph_point, start,end);
		}
	}
	return 0;
}

int give_param_list(graph_list* graph,int start,int end)
{
	graph_list* arc_tmp=graph+start;
	while(arc_tmp->next!=NULL&&arc_tmp->to!=-1)
	{
		if(arc_tmp->to==end)
		{
			return arc_tmp->size;
		}
		else
		{
			arc_tmp=arc_tmp->next;
		}
	}
	return 0;
}

int give_param_matrix(int** graph, int start, int end)
{
	return graph[start][end];
}

int give_param(graph_user graph_usr, int start, int end)
{
	if(safety(graph_usr,0)==1)
	{
		graph_user_var* graph=(graph_user_var*)graph_usr;
		if((start<0||start>=graph->length)||(end<0||end>=graph->length))
		{
			return 0;
		}
		if(graph->type==1)		//list
		{
			return give_param_list((graph_list*)(graph->graph_point),start,end);
		}
		if(graph->type==2)		//matrix
		{
			return give_param_matrix((int**)graph->graph_point,start,end);
		}
	}
	return 0;
}

graph_user copy_graph(graph_user graph_usr)
{
	if(safety(graph_usr,0)==1)
	{
		int i=0;
		graph_user_var* graph=(graph_user_var*)graph_usr;
		graph_user_var* new_graph=(graph_user_var*)make_graph(graph->type,graph->length,graph->orient,graph->weight);
		if(graph->type==1)		//list
		{
			graph_list* arcs_tmp=(graph_list*)graph->graph_point;
			for(i=0;i<(graph->length);i++)
			{
				arcs_tmp=(graph_list*)graph->graph_point+i;
				while(arcs_tmp->next!=NULL)
				{
					add_arc_list((graph_list*)new_graph->graph_point,i,arcs_tmp->to,arcs_tmp->size);
					arcs_tmp=arcs_tmp->next;
				}
			}
			return (graph_user)new_graph;
		}
		if(graph->type==2)		//matrix
		{
			int i=0,j=0;
			int** graph_tmp1=new_graph->graph_point;
			int** graph_tmp2=graph->graph_point;
			for(i=0;i<graph->length;i++)
			{
				for(j=0;j<graph->length;j++)
				{
					graph_tmp1[i][j]=graph_tmp2[i][j];
				}				
			}
			return (graph_user)new_graph;
		}
	}
	return NULL;
}

void delete_graph_list(graph_list* graph, int length)
{
	graph_list* arcs_tmp1;
	graph_list* arcs_tmp2;
	int i=0;
	for(i=0;i<length;i++)
	{
		arcs_tmp1=graph+i;
		while(arcs_tmp1->next!=NULL)
		{
			arcs_tmp2=arcs_tmp1;
			arcs_tmp1=arcs_tmp1->next;
			free(arcs_tmp2);
		}
		free(arcs_tmp1);

	}
	free(graph);
}

void delete_graph_matrix(int** graph,int length)
{
	int i=0;
	for(i=0;i<length;i++)
	{
		free(*(graph+i));
	}
	free(graph);
}

graph_user delete_graph(graph_user graph_usr)
{
	if(safety(graph_usr,0)==1)
	{
		graph_user_var* graph=(graph_user_var*)graph_usr;
		//safety(graph_usr,2);
		if(graph->type==1)		//list
		{
			delete_graph_list((graph_list*)graph->graph_point,graph->length);
			free(graph);
			safety(graph_usr,2);
			return NULL;
		}
		if(graph->type==2)		//matrix
		{
			delete_graph_matrix((int**)graph->graph_point,graph->length);
			free(graph);
			safety(graph_usr,2);
			return NULL;
		}
	}
}

graph_user load_graph(char* file_addr, char type)
{
	FILE* input = fopen(file_addr,"r");
	int length, orient, weight;
	fscanf(input,"%d %d %d",&length, &orient, &weight);
	if(type!=2&&type!=1)
	{
		return NULL;
	}
	if(orient!=1&&orient!=0&&weight!=1&&orient!=0&&weight<0)
	{
		return NULL;
	}
	graph_user_var* graph = (graph_user_var*)make_graph(type,length,(char)orient,(char)weight);
	if(weight==0)
	{
		int end=0, start=0;
		while(!feof(input))
		{
			fscanf(input,"%d %d",&start, &end);
			add_arc((graph_user)graph,start-1,end-1,1);
		}
	}
	else
	{
		int end=0, start=0, size = 0;
		while(!feof(input))
		{
			fscanf(input,"%d %d %d",&start, &end, &size);
			add_arc((graph_user)graph,start-1,end-1,size);
		}
	}
	fclose(input);
	return graph;
}

void save_graph(char* file_addr, graph_user graph_usr)		
{
	if(safety(graph_usr,0)==1)
	{
		graph_user_var* graph=(graph_user_var*)graph_usr;
		FILE* output = fopen(file_addr,"w");
		fprintf(output, "%d %d %d\n", graph->length, (int)graph->orient, (int)graph->weight);
		if(graph->type==1)		//list
		{
			if(graph->orient==1)
			{
				int i=0;
				graph_list* graph_tmp=graph->graph_point;
				for (i=0;i<graph->length;i++)
				{
					graph_tmp=(graph_list*)(graph->graph_point)+i;
					while(graph_tmp->next!=NULL&&graph_tmp->to!=-1)
					{
						if(graph->weight==1)
						{
							if(graph_tmp->to!=-2)
							{
								fprintf(output, "%d %d %d\n", i+1, graph_tmp->to+1, graph_tmp->size);
							}
						}
						else
						{
							if(graph_tmp->to!=-2)
							{
								fprintf(output, "%d %d\n", i+1, graph_tmp->to+1);
							}
						}
						graph_tmp=graph_tmp->next;
					}
				}
				fclose(output);
				return;
			}
			else
			{
				graph_user_var* graph_flags=(graph_user_var*)copy_graph(graph_usr);
				int i=0;
				graph_list* graph_tmp=(graph_list*)graph_flags->graph_point;
				for (i=0;i<graph->length;i++)
				{
					graph_tmp=(graph_list*)(graph_flags->graph_point)+i;
					while(graph_tmp->next!=NULL&&graph_tmp->to!=-1)
					{
						if(graph->weight==1)
						{
							if(graph_tmp->to!=-2)
							{
								fprintf(output, "%d %d %d\n", i+1, graph_tmp->to+1, graph_tmp->size);
								delete_arc(graph_flags,i,graph_tmp->to);
							}
						}
						else
						{
							if(graph_tmp->to!=-2)
							{
								fprintf(output, "%d %d\n", i+1, graph_tmp->to+1);
								delete_arc(graph_flags,i,graph_tmp->to);
							}
						}
						graph_tmp=graph_tmp->next;
					}
				}
				delete_graph(graph_flags);
				fclose(output);
				return;
			}
		}
		if(graph->type==2)		//matrix
		{
			int i=0,j=0;
			if(graph->orient==0)
			{
				int** flags=calloc(graph->length,sizeof(int*));
				for(i=0;i<graph->length;i++)
				{
					*(flags+i)=calloc(graph->length,sizeof(int));
				}
				int** graph_tmp=graph->graph_point;
				for(i=0;i<graph->length;i++)
				{
					for(j=0;j<graph->length;j++)
					{
						if(graph_tmp[i][j]!=0)
						{
							if(flags[i][j]!=1)
							{
								if(graph->weight==1)
								{
									fprintf(output, "%d %d %d\n", i+1, j+1, graph_tmp[i][j]);
								}
								else
								{
									fprintf(output, "%d %d\n", i+1, j+1);
								}
								flags[i][j]=1;
								flags[j][i]=1;
							}
						}
					}
				}
				for(i=0;i<graph->length;i++)
				{
					free(*(flags+i));
				}
				free(flags);
			}
			else
			{
				int** graph_tmp=graph->graph_point;
				for(i=0;i<graph->length;i++)
				{
					for(j=0;j<graph->length;j++)
					{
						if(graph_tmp[i][j]!=0)
						{
							if(graph->weight==1)
							{
								fprintf(output, "%d %d %d\n", i+1, j+1, graph_tmp[i][j]);
							}
							else
							{
								fprintf(output, "%d %d\n", i+1, j+1);
							}
						}
					}
				}
			}
			fclose(output);
			return;
		}
	}
}

int give_length(graph_user graph_usr)
{
	if(safety(graph_usr,0)==1)
	{
		graph_user_var* graph=(graph_user_var*)graph_usr;
		return graph->length;
	}
	return 0;
}