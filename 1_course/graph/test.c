#include "graph.h"

int main(int argc,char* argv[])
{
	graph_user graph;
	if(argc!=2)
	{
		puts("RUN ERROR!");
		return 1;
	}
	graph=load_graph(argv[1],1);
	graph_user graph2=copy_graph(graph);
	printf("%d -> %d  %d\n",1,5,check_arc(graph,1,5));
	add_arc(graph,1,5,1);
	printf("%d -> %d  %d\n",1,4,check_arc(graph2,1,4));
	delete_arc(graph2,1,4);
	printf("%d -> %d  %d\n",1,4,check_arc(graph2,1,4));
	delete_arc(graph2,1,4);
	add_arc(graph,1,10,1);
	printf("%d -> %d  %d\n",1,10,check_arc(graph2,1,10));
	save_graph("out1.txt",graph);
	save_graph("out2.txt",graph2);
	graph = delete_graph(graph);
	graph = delete_graph(graph);
	graph2 = delete_graph(graph2);
	graph2 = delete_graph(graph2);
}