#define CTRL_IN LIBUSB_ENDPOINT_IN|LIBUSB_REQUEST_TYPE_CLASS|LIBUSB_RECIPIENT_INTERFACE
#define HID_GET_IDLE 0x02

#include <iostream>
#include "libusb.h"
#include <iomanip>
#include <cstring>
 
void printdev(libusb_device *dev);
 
int main(){
 
        libusb_device **devs;
        libusb_context *ctx = NULL;
       
        int r;
       
        ssize_t cnt;
        ssize_t i;
 
        std::cout.setf(std::ios::left);
 
        if ( (r = libusb_init(&ctx)) < 0) {
                std::cerr << "Ошибка: инициализация не выполнена, код: " << r << std::endl;
                return 1;
        }  
 
        libusb_set_debug(ctx, 3);
 
        if ( (cnt = libusb_get_device_list(ctx, &devs)) < 0 ){
                std::cerr << "Ошибка: список USB устройств не получен." << std::endl;  
                return 1;
        }        
 
        std::cout <<   std::setw(10) << "Class"
                << std::setw(15) << "VendorID"
                << std::setw(15) << "ProductID"
                << "Serial Number" << std::endl << std::endl;
 
        for(i = 0; i < cnt; i++) {
                printdev(devs[i]);
        }
 
        libusb_free_device_list(devs, 1);
        libusb_exit(ctx);
 
        return 0;
 
}
 
void printdev(libusb_device *dev){
 
        libusb_device_descriptor desc;
        libusb_config_descriptor *config;
        libusb_device_handle* handle = NULL;
 
        char str[256];
        char str2[256];
 
        int r = 0;
 
        if ((r = libusb_get_device_descriptor(dev, &desc)) < 0){
                std::cerr << "Ошибка: дескриптор устройства не получен, код: " << r;
                return;
        }
 
        if ((r = libusb_open(dev, &handle)) < 0){
                std::cerr << "Ошибка: невозможно получить доступ к устройству, код: " << r;
                return;
        }
 
        if ((r = libusb_get_string_descriptor_ascii(handle,
                         desc.iManufacturer, (unsigned char*) str, sizeof(str))) < 0){
                libusb_close(handle);
                return;
        }

        // if ((r = libusb_get_string_descriptor_ascii(handle,
        //                  desc.iSerialNumber, (unsigned char*) str2, sizeof(str2))) < 0){
        //         libusb_close(handle);
        //     return;
        // }
 
        libusb_get_config_descriptor(dev, 0, &config);
        
        char buffer[256];
        //int result = libusb_control_transfer(handle, CTRL_IN, HID_GET_IDLE, 0, 0, (unsigned char *)buffer, strlen(buffer), 5000);

        std::cout <<   std::setw(10) << (int)desc.bDeviceClass
                << std::setw(15) << desc.idVendor
                << std::setw(15) << desc.idProduct
                //<< std::setw(15) << str2
                << str << std::endl;
 
    libusb_close(handle);
}