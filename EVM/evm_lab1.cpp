#include <iostream>
#include <math.h>
#include <sys/times.h>
#include <unistd.h>

using namespace std;

int main()
{
    struct tms start, end;
    long clocks_per_sec = sysconf(_SC_CLK_TCK);
    long clocks;
    long int N=175000000;
    int k=3;
    while(k>0)
    {
        k--;
        N+=87500000;
        /*cout<<"Enter the number of intervals: ";
        cin>>N;*/
        double rez=0;
        times(&start);
        for (double i=0;i<M_PI;i+=M_PI/N)
        {
            rez+=(exp(i)*sin(i)+exp(i+M_PI/N)*sin(i+M_PI/N))/2;
        }
        rez*=M_PI/N;
        times(&end);
        cout<<rez<<endl;
        clocks = end.tms_utime - start.tms_utime;
        cout<<"Time taken: "<<(double)clocks / clocks_per_sec<<"sec."<<endl;
    }
    return 0;
}
