#include "Planar.h"
std::vector<std::tuple<Point, uint>> Planar::LookUp()
{
	std::vector<std::tuple<Point, uint>> lookedUp;
	int a[4] = { -10,-1,1,10 };
	Point currentPoint = { this->current.x,this->current.y };
	int sizeX = this->field[currentPoint.y].size();
	int sizeY = this->field.size();
	uint j = 0;
	for (uint i = 0; i<4; i++)
	{
		if (((int)currentPoint.y + (a[i] / 10)<sizeY) && ((int)currentPoint.x + (a[i] % 10)<sizeX)
			&& (currentPoint.y + (a[i] / 10) >= 0) && (currentPoint.x + (a[i] % 10) >= 0))
		{
			if (this->field[currentPoint.y + (a[i] / 10)][currentPoint.x + (a[i] % 10)] == '.')
			{
				Point nextPoint = { currentPoint.x + (a[i] % 10),currentPoint.y + (a[i] / 10) };
				uint distance = this->Distance(nextPoint,GetFinish());
				lookedUp.push_back(std::make_tuple(nextPoint, distance));
				j++;
			}
			if (this->field[currentPoint.y + (a[i] / 10)][currentPoint.x + (a[i] % 10)] == 'F')
			{
				Point nextPoint = { currentPoint.x + (a[i] % 10),currentPoint.y + (a[i] / 10) };
				uint distance = this->Distance(nextPoint, GetFinish());
				lookedUp.push_back(std::make_tuple(nextPoint, 0));
				j++;
			}
		}
	}
	return lookedUp;
}

uint Planar::Distance(const Point from,const Point to)
{
	return abs((int)from.x - (int)to.x) + abs((int)from.y - (int)to.y);
}
