#pragma once
#include "Surface.h"

typedef unsigned int uint;

struct Point
{
	uint x;
	uint y;
};

class Planar :
	public SpaceImple<Point, uint>
{
public:
	virtual std::vector <std::tuple<Point, uint>> LookUp();
	virtual uint Distance(const Point from, const Point to);
};

