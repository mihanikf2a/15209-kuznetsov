#pragma once

#include <iostream>
#include <vector>
#include <tuple>
#include <fstream>
#include <string>
#include <limits>
#include <ctime>
#include <cmath>

const int _OUTPUT_FILE_OPEN_ERROR_ = -100;
const int _INPUT_FILE_OPEN_ERROR_ = -101;
const int _UNKNOWN_TOPOLOGY_TYPE_ = -102;
const int _BADMOVE_EXCEPTION_ = -103;
const int _GT_ALLOWABLE_LIMIT_ = -104;
const int _DICTIONARY_FILE_OPEN_ERROR = -105;

const int _HELP_CALLED_ = 101;

typedef unsigned int		uint;
typedef unsigned char		uchar;

struct Config {
	bool help = false;
	std::string space = "space.txt";
	std::string out = "route.txt";
	std::string dict = "dictionary.txt";
	int limit = 1000;
	std::string topology = "planar";
};

class BadMove : public std::exception {};
class FileError : public std::exception {};

Config ReadParams(int argc, char** argv);
void CallHelp();

template <typename P, typename M>
class SpaceImple
{
protected:
	P start;
	P finish;
	P current;
	std::vector<std::string> field;
public:
	virtual M Move(const P point);
	virtual std::vector<std::tuple<P, M>> LookUp() = 0;
	virtual M Distance(const P from, const P to) = 0;
	P GetCurrent() const;
	P GetFinish() const;

	friend std::ostream& operator<<(std::ostream &to, SpaceImple<P, M> &space);
	friend std::istream& operator >> (std::istream &from, SpaceImple<P, M> &space);

};

template<class P, typename M>
class Robot {
	SpaceImple<P, M> *space;

public:
	Robot(SpaceImple<P, M> *space) : space(space) {};

	void Discover();
	P GetCloses(const std::vector<std::tuple<P, M>> points);
	std::ostream ShowField(std::ostream &to);

};
template<typename P,typename M>
std::ostream& operator<<(std::ostream &to, SpaceImple<P, M> &space);
template<typename P, typename M>
std::istream& operator >> (std::istream &from, SpaceImple<P, M> &space);
