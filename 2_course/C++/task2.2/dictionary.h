#pragma once
#include "Surface.h"

class Dictionary 
	: public SpaceImple<std::string, uint> 
{
	std::string dictionary;
	std::vector<std::vector<std::string>> cache;
	bool isRead;
public:
	Dictionary(std::istream &from, std::string dictFile);
	~Dictionary();

	virtual uint Distance(std::string &from, std::string &to);
	virtual uint Move(std::string &word);
	virtual std::vector<std::tuple<std::string, uint>> LookUp();
	friend std::istream& operator >> (std::istream &from, Dictionary &space);
};

std::istream& operator >> (std::istream &from, Dictionary &space);