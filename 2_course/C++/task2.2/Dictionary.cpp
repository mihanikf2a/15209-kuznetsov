#include "dictionary.h"

Dictionary::Dictionary(std::istream &from, std::string dictFile)
{
	from >> *this;

	this->dictionary = dictFile;
	this->isRead = false;
	this->cache.reserve(Distance(start, finish) - 1);

	this->field.push_back(start);
}

Dictionary::~Dictionary()
{
	cache.clear();
	field.clear();

	cache.~vector();
	field.~vector();
}

uint Dictionary::Distance(std::string &from, std::string &to)
{
	const uint len1 = from.size(),
		len2 = to.size();

	std::vector<uint> col(len2 + 1),
		prevCol(len2 + 1);

	for (uint i = 0; i < prevCol.size(); i++)
	{
		prevCol[i] = i;
	}

	for (uint i = 0; i < len1; i++)
	{
		col[0] = i + 1;

		for (uint j = 0; j < len2; j++)
		{
			col[j + 1] = (uint)fmin(fmin(prevCol[1 + j] + 1, col[j] + 1), prevCol[j] + (from[i] == to[j] ? 0 : 1));
		}
		col.swap(prevCol);
	}

	return prevCol[len2];
}

std::vector<std::tuple<std::string, uint>> Dictionary::LookUp()
{
	std::vector<std::tuple<std::string, uint>> result;

	uint maxDest = Distance(start, finish);
	uint distance = SIZE_MAX;
	std::ifstream dictFile;
	std::string word;
	uint currInd;

	if (!isRead)
	{
		dictFile.open(dictionary);

		if (!dictFile.good())
		{
			throw FileError();
		}
		while (!dictFile.eof())
		{
			dictFile >> word;

			if (((distance = Distance(start, word)) > 0) && (distance <= cache.size()) && (Distance(word, finish) < Distance(start, finish)))
			{
				cache[distance - 1].push_back(word);
			}

		}

		dictFile.close();
		isRead = true;
	}

	distance = Distance(current, finish);
	currInd = Distance(start, finish) - distance;

	for (uint i = 0; i < cache[currInd].size(); ++i)
	{
		result.push_back(make_tuple(cache[currInd][i], currInd + 1));
	}

	return result;
}
uint Dictionary::Move(std::string &word)
{
	if (word == start || word == current)
	{
		throw BadMove();
	}

	current = word;
	field.push_back(word);

	return Distance(word, finish);
}

void Robot<std::string, uint>::Discover()
{
	std::string next;
	do
	{
		next = GetCloses(space->LookUp());
	} while (space->Move(next));
}

std::istream& operator >> (std::istream &from, Dictionary &space)
{
	from >> space.start;
	from >> space.finish;
	space.current = space.start;

	space.cache = std::vector<std::vector<std::string>>(space.Distance(space.start, space.finish));

	return from;
}