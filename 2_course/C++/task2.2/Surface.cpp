#include "Surface.h"
#include <string>

template<typename P, typename M>
M SpaceImple<P,M>::Move(const P point)
{
	this->current = point;
	return this->DistanceToFinish(point);
}

template<typename P, class M>
P SpaceImple<P,M>::GetCurrent() const
{
	return this->current;
}
template<typename P, class M>
P SpaceImple<P, M>::GetFinish() const
{
	return this->finish;
}

template<class P, typename M>
void Robot<P, M>::Discover()
{
	std::tuple<P, M> next;
	P pNext;
	M dist;
	do {
		pNext = this->GetCloses(space->LookUp());
		dist = space->Move(pNext);
	} while (dist);
}

template<class P, typename M>
P Robot<P, M>::GetCloses(const std::vector<std::tuple<P, M>> points)
{
	if (points.empty())
	{
		throw BadMove();
	}
	M minDistance = std::get<1>(points[0]);
	P soonPoint = std::get<0>(points[0]);
	for (uint i = 0; i < points.size(); ++i)
	{
		if (minDistance > std::get<1>(points[i]))
		{
			minDistance = std::get<1>(points[i]);
			soonPoint = std::get<0>(points[i]);
		}
	}
	return soonPoint;
}

template<class P, typename M>
std::ostream Robot<P, M>::ShowField(std::ostream & to)
{
	to << *(this->space);
	return to;
}

template<typename P, typename M>
std::ostream& operator<<(std::ostream &to, SpaceImple<P, M> &space)
{
	for (int i = 0; i < (space->field).size; i++)
	{
		to << space->field[i];
	}
	return to;
}
template<typename P, typename M>
std::istream& operator >> (std::istream &from, SpaceImple<P, M> &space)
{
	while (!from.eof())
	{
		std::string fromString;
		from >> fromString;
		space.field.push_back(fromString);
	}
	return from;
}
