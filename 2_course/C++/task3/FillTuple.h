#pragma once
#include "Parser.h"

template<class Tuple, std::size_t IND>
struct tuple_filler 
{
	static void fill_tuple(Tuple tupl, std::string line, const csv::_seps &seps, csv::_rerr &rErr)
	{
		if (line.empty())
			return;

		std::string val;

		if (line[line.size() - 1] == seps.scr)
		{
			line.pop_back();

			val = line.substr(line.find_last_of(seps.scr) + 1, line.size() - line.find_last_of(seps.scr));

			line.erase(line.find_last_of(seps.scr), line.size() - line.find_last_of(seps.scr));

			while (line[line.size() - 1] == seps.scr)
			{
				std::string subs;
				line.pop_back();
				subs = line.substr(line.find_last_of(seps.scr) + 1, line.size() - line.find_last_of(seps.scr));

				subs.push_back(seps.scr);

				val = (subs += val);
				line.erase(line.find_last_of(seps.scr), line.size() - line.find_last_of(seps.scr));
			}
			line.pop_back();
		}
		else
		{
			val = line.substr(line.find_last_of(seps.deler) + 1, line.size() - line.find_last_of(seps.deler));

			line.erase(line.find_last_of(seps.deler), line.size() - line.find_last_of(seps.deler));
		}

		rErr.col += 1;
		tuple_filler<Tuple, IND - 1>::fill_tuple(tupl, line, seps, rErr);

		if (typeid(get<IND - 1>(tupl)) == typeid(std::string))
		{
			get<IND - 1>(tupl) = (decltype(get<IND - 1>(tupl)))val;
		}
		else
		{
			std::stringstream str(val);

			auto tmp = get<IND - 1>(tupl);
			str >> tmp;

			if (str.fail()) 
			{
				rErr.col = rErr.width - IND;
				throw csv::ReadException();
				return;
			}

			get<IND - 1>(tupl) = tmp;
		}
	}
};

template<class Tuple>
struct tuple_filler<Tuple, 1> 
{
	static void fill_tuple(Tuple tupl, std::string line, const csv::_seps &seps, csv::_rerr &rErr)
	{
		string str;

		if (line[line.size() - 1] == seps.scr)
		{
			line.pop_back();

			str = line.substr(line.find_last_of(seps.scr) + 1, line.size() - line.find_last_of(seps.scr));

			line.erase(line.find_last_of(seps.scr), line.size() - line.find_last_of(seps.scr));

			while (line[line.size() - 1] == seps.scr)
			{
				string subs;
				line.pop_back();
				subs = line.substr(line.find_last_of(seps.scr) + 1, line.size() - line.find_last_of(seps.scr));

				subs.push_back(seps.scr);

				str = subs += str;
				line.erase(line.find_last_of(seps.scr), line.size() - line.find_last_of(seps.scr));
			}
			line.pop_back();
		}
		else
		{
			str = line;
		}

		if (typeid(get<0>(tupl)) == typeid(string))
		{
			get<0>(tupl) = (decltype(get<0>(tupl)))str;
		}
		else
		{
			stringstream str(str);

			auto tmp = get<0>(tupl);

			str >> tmp;

			if (str.fail())
			{
				rErr.col = rErr.width - 1;
				throw csv::ReadException();
				return;
			}

			get<0>(tupl) = tmp;
		}

		rErr.col = 0;
	}

};

template<class... Args>
void fill_tuple(std::tuple<Args...> &t, std::string line, csv::_seps seps, csv::_rerr &rErr)
{
	tuple_filler<decltype(t), sizeof...(Args)>::fill_tuple(t, line, seps, rErr);
}