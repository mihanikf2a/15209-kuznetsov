#include <list>
#include <fstream>
#include <iostream>

#include "mySort.hpp"

using namespace std;

int main(int argc, char** argv)
{
	if(argc!=3)
	{
		cout<<"Bad run command!";
		return 1;
	}
	ifstream fileIn;
	fileIn.open(argv[1]);
	if(!fileIn.is_open())
	{
		cout<<"Error open input file!";
		return 1;
	}
	list<string> textFromFile;
	while(!fileIn.eof())
	{
		string tempString;
		getline(fileIn,tempString);
		textFromFile.push_back(tempString);
	}
	fileIn.close();
	mySortList(textFromFile);
	ofstream fileOut;
	fileOut.open(argv[2]);
	if(!fileOut.is_open())
	{
		cout<<"Error open output file!";
		return 1;
	}
	while(!textFromFile.empty()) 
	{
		fileOut<<textFromFile.front()<<endl;
        textFromFile.pop_front();
    }
	fileOut.close();
	return 0;
}