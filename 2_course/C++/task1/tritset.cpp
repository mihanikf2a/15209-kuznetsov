#include "tritset.h"
#include <iostream>

typedef unsigned short ushort;

const uint _FALSE_ = 0x0;
const uint _UNKNOWN_ = 0x1;
const uint _TRUE_ = 0x3;

const uint _UNKNOWN_BLOCK_ = 0x55555555;

const ushort _TRITS_IN_UINT_ = 4 * sizeof(uint);

using namespace Trits;

TritSet::TritSet(int newSize) : capacity(0), size(1)
{
	this->trits = new uint[1];
	this->trits[0] = _UNKNOWN_BLOCK_;
}
TritSet::TritSet(const TritSet& origTrits) : capacity(origTrits.capacity), size(origTrits.size)
{
	this->trits = new uint[this->size];
	for (int i = 0; i<this->size; i++)
	{
		this->trits[i] = origTrits.trits[i];
	}
}

TritSet::TritSet(TritSet&& origTrits) : capacity(origTrits.capacity), size(origTrits.size)
{
	origTrits.capacity = 0;
	origTrits.size = 0;
	delete[] origTrits.trits;
	this->trits = origTrits.trits;
	origTrits.trits = nullptr;
}

TritSet::~TritSet()
{
	this->size = 0;
	this->capacity = 0;
	delete[] this->trits;
	this->trits = nullptr;
	delete this;
}

TritSet::TritItem::TritItem(int newIndex, TritSet& newParent) : index(newIndex), parent(newParent)
{
}

TritSet::TritItem::~TritItem()
{
	delete this;
}

TritSet::TritItem TritSet::operator[] (int index)
{
	return TritItem(index, *this);
}

Trit TritSet::GetTrit(int index) const
{
	if (index>capacity)
	{
		return Unknown;
	}
	uint needBlock = this->trits[index / _TRITS_IN_UINT_];
	for (int i = 0; i<(index%_TRITS_IN_UINT_); i++)
	{
		needBlock >>= 2;
	}
	needBlock &= _TRUE_;
	switch (needBlock)
	{
	case _TRUE_: return True; break;
	case _FALSE_: return False; break;
	case _UNKNOWN_: return Unknown; break;
	default: return Unknown; break;
	}
}

void TritSet::SetTrit(int index, Trit newValue)
{
	if (capacity<index)
	{
		if (newValue == Unknown)
		{
			return;
		}
		int newSize = (index / _TRITS_IN_UINT_) + !!(index%_TRITS_IN_UINT_);
		uint* temp = new uint[newSize];
		for (int i = 0; i<this->size; i++)
		{
			temp[i] = this->trits[i];
		}
		for (int i = this->size; i<newSize; i++)
		{
			temp[i] = _UNKNOWN_BLOCK_;
		}
		delete[] this->trits;
		this->trits = temp;
		this->size = newSize;
		this->capacity = index;
	}
	uint changeNum;
	uint killer = _TRUE_;
	switch (newValue)
	{
	case False:
		changeNum = _FALSE_;
		break;
	case Unknown:
		changeNum = _UNKNOWN_;
		break;
	case True:
		changeNum = _TRUE_;
		break;
	}
	for (int shift = 0; shift < (index%_TRITS_IN_UINT_); shift++)
	{
		changeNum <<= 2;
		killer <<= 2;
	}
	this->trits[index / _TRITS_IN_UINT_] &= ~killer;
	this->trits[index / _TRITS_IN_UINT_] |= changeNum;
}

TritSet::TritItem::operator Trit()
{
	return this->parent.GetTrit(this->index);
}

Trit TritSet::TritItem::operator= (Trit newValue)
{
	this->parent.SetTrit(this->index, newValue);
	return newValue;
}

Trit TritSet::TritItem::operator= (const TritItem& newItem)
{
	Trit newValue = newItem.parent.GetTrit(newItem.index);
	this->parent.SetTrit(this->index, newValue);
	return newValue;
}

TritSet& TritSet::operator=(const TritSet& secondTrit)
{
	if (this == &secondTrit)
	{
		return *this;
	}
	delete this;
	*this = TritSet(secondTrit);
	return *this;
}

TritSet& TritSet::operator=(TritSet&& secondTrit)
{
	if (this == &secondTrit)
	{
		return *this;
	}
	delete[] this->trits;
	this->capacity = secondTrit.capacity;
	this->size = secondTrit.size;
	this->trits = secondTrit.trits;
	secondTrit.capacity = 0;
	secondTrit.size = 0;
	secondTrit.trits = nullptr;
	return *this;
}

TritSet& TritSet::operator|=(const TritSet& secondTrit)
{
	int maxLen = (this->size > secondTrit.size) ? this->size : secondTrit.size;
	int minLen = (this->size < secondTrit.size) ? this->size : secondTrit.size;
	uint *bigger = (this->size > secondTrit.size) ? this->trits : (&secondTrit)->trits;
	uint *newArr;
	if (maxLen > this->size)
	{
		newArr = new uint[maxLen];
		for (int i = 0; i < this->size; i++)
		{
			newArr[i] = this->trits[i];
		}
		for (int i = this->size; i < maxLen; i++)
		{
			newArr[i] = _UNKNOWN_BLOCK_;
		}
		delete[] this->trits;
		this->trits = newArr;
		this->size = maxLen;
		this->capacity = secondTrit.capacity;
	}
	if (this->capacity<secondTrit.capacity)
	{
		this->capacity = secondTrit.capacity;
	}
	for (int i = 0; i < minLen; i++)
	{
		this->trits[i] = this->trits[i] | secondTrit.trits[i];
	}
	for (int i = minLen; i < maxLen; i++)
	{
		this->trits[i] = _UNKNOWN_BLOCK_ | bigger[i];
	}
	for (int i = (this->size)*_TRITS_IN_UINT_; i != 0; i--)
	{
		if (this->GetTrit(i - 1) != Unknown)
		{
			this->capacity = i-1;
			break;
		}
	}
	return *this;
}

TritSet& TritSet::operator&=(const TritSet& secondTrit)
{
	int maxLen = (this->size > secondTrit.size) ? this->size : secondTrit.size;
	int minLen = (this->size < secondTrit.size) ? this->size : secondTrit.size;
	uint *bigger = (this->size > secondTrit.size) ? this->trits : (&secondTrit)->trits;
	uint *newArr;
	if (maxLen > this->size)
	{
		newArr = new uint[maxLen];
		for (int i = 0; i < this->size; i++)
		{
			newArr[i] = this->trits[i];
		}
		for (int i = this->size; i < maxLen; i++)
		{
			newArr[i] = _UNKNOWN_BLOCK_;
		}
		delete[] this->trits;
		this->trits = newArr;
		this->size = maxLen;
		this->capacity = secondTrit.capacity;
	}
	if (this->capacity<secondTrit.capacity)
	{
		this->capacity = secondTrit.capacity;
	}
	for (int i = 0; i < minLen; i++)
	{
		this->trits[i] = this->trits[i] & secondTrit.trits[i];
	}
	for (int i = minLen; i < maxLen; i++)
	{
		this->trits[i] = _UNKNOWN_BLOCK_ & bigger[i];
	}
	for (int i = (this->size)*_TRITS_IN_UINT_; i > -1; i--)
	{
		if ((this->GetTrit(i - 1) != Unknown)&&(i!=0))
		{
			this->capacity = i - 1;
			break;
		}
		if (i == 0)
		{
			this->capacity = 0;
		}
	}
	return *this;
}

TritSet Trits::operator| (const TritSet& firstTrit, const TritSet& secondTrit)
{
	TritSet result = firstTrit;
	result |= secondTrit;
	return result;
}

TritSet Trits::operator& (const TritSet& firstTrit, const TritSet& secondTrit)
{
	TritSet result = firstTrit;
	result &= secondTrit;
	return result;
}

TritSet TritSet::operator~() const
{
	Trit currentTrit;
	TritSet result = *this;
	for (int i = 0; i <= this->capacity; i++)
	{
		currentTrit = this->GetTrit(i);
		if (currentTrit == Unknown)
		{
			continue;
		}
		if (currentTrit == False)
		{
			result.SetTrit(i, True);
		}
		else
		{
			result.SetTrit(i, False);
		}
	}
	return result;
}

int TritSet::GetNumOfTrit() const
{
	return this->capacity;
}

int TritSet::GetSizeOfArray() const
{
	return this->size;
}

int TritSet::Cardinality(Trit value) const
{
	int cardinality = 0;
	if (this->capacity != 0)
	{
		for (int i = 0; i <= this->capacity; i++)
		{
			if (this->GetTrit(i) == value)
			{
				cardinality++;
			}
		}
	}
	return cardinality;
}
int TritSet::GetLength() const
{
	return this->capacity + 1;
}

void TritSet::Trim(int lastIndex)
{
	if (lastIndex > this->capacity)
	{
		return;
	}
	int newSize = (lastIndex / _TRITS_IN_UINT_) + !!(lastIndex%_TRITS_IN_UINT_);
	for (int i = lastIndex-1; i>-1; i--)
	{
		if (this->GetTrit(i) != Unknown || i == 0)
		{
			this->capacity = i;
			break;
		}
	}
	newSize = (((this->capacity+1) / _TRITS_IN_UINT_) + !!((this->capacity+1)%_TRITS_IN_UINT_));
	uint* newTrits = new uint[newSize];
	for (int i = 0; i<newSize; i++)
	{
		newTrits[i] = this->trits[i];
	}
	delete[] this->trits;
	this->trits = newTrits;
	this->size = newSize;
}
TritSet TritSet::Shrink()
{
	if (this->capacity == 0)
	{
		return *this;
	}
	int shift = 0;
	for (int i = 0; i<=this->capacity; i++)
	{
		if (this->GetTrit(i) != Unknown)
		{
			shift = i;
			break;
		}
	}
	int newSize = (this->capacity) - shift;
	TritSet newTrits(newSize);
	for (int i = 0; i <= newSize; i++)
	{
		newTrits.SetTrit(i, this->GetTrit(i + shift));
	}
	return newTrits;
}

std::unordered_map<Trit, int, std::hash<int>> TritSet::Cardinality() const
{
	std::unordered_map<Trit, int, std::hash<int>> res = { { True, 0 },{ False, 0 },{ Unknown, 0 } };
	for (int i = 0; i <= this->capacity; i++)
	{
		res.at(this->GetTrit(i))++;
	}
	return res;
}

std::ostream& operator<<(std::ostream& outStream, const Trit& value)
{
	switch (value)
	{
	case True: outStream << "True"; break;
	case False: outStream << "False"; break;
	case Unknown: outStream << "Unknown"; break;
	}
	return outStream;
}

int TritSet::Capacity() const 
{
	return this->size;
}