#include "gtest\gtest.h"
#include "tritset.h"

typedef unsigned short ushort;

const ushort _TRITS_IN_UINT_ = 4 * sizeof(uint);

using namespace Trits;

TEST(CapacityTest, ArrLenNotEqualCapacityRes)
{
	TritSet setA(1000);

	ASSERT_EQ(setA.Capacity(), 1);
	ASSERT_GE(setA.Capacity(), 1);
}

TEST(GetNumOfTritsTest, FalseNumOfTrits) 
{
	TritSet set(1000);

	ASSERT_EQ(set.GetNumOfTrit(), 0);
}

TEST(CardinalitySingleTest, IncorrectCardinalitySingleRes) 
{
	TritSet set(1000);

	set[0] = False;
	set[9] = True;
	set[12] = True;
	set[14] = True;

	ASSERT_EQ(set.Cardinality(True), 3);
	ASSERT_EQ(set.Cardinality(False), 1);
	ASSERT_EQ(set.Cardinality(Unknown), 11);
}

TEST(CardinalityMultiplyTest, IncorrectCardinalityMultiplyRes) 
{
	TritSet set(1000);
	std::unordered_map<Trit, int, std::hash<int>> res;

	set[0] = False;
	set[9] = True;
	set[12] = True;
	set[14] = True;

	res = set.Cardinality();

	ASSERT_EQ(res.at(True), 3);
	ASSERT_EQ(res.at(False), 1);
	ASSERT_EQ(res.at(Unknown), 11);
}

TEST(LengthTest, IncorrectLengthRes)
{
	TritSet set(_TRITS_IN_UINT_ * 100);

	set[0] = False;
	ASSERT_EQ(set.GetLength(), 1);

	set[1612] = Unknown;
	ASSERT_EQ(set.GetLength(), 1);

	set[201] = True;
	ASSERT_EQ(set.GetLength(), 202);

	set[1599] = True;
	ASSERT_EQ(set.GetLength(), 1600);
}

TEST(TrimTest, IncorrectTrimRes)
{
	TritSet set(_TRITS_IN_UINT_ * 100);

	set[8] = True;
	Trit b = set[8];
	ASSERT_EQ(True, b);

	set.Trim(100);

	ASSERT_EQ(set.GetLength(), 9);

	set.Trim(8);
	ASSERT_EQ(set.GetLength(), 1);
}

TEST(ShrinkTest, IncorrectShrinkRes) 
{
	TritSet set(_TRITS_IN_UINT_ * 100);

	set[100] = True;
	ASSERT_EQ(set.GetLength(), 101);

	TritSet res = set.Shrink();
	ASSERT_EQ(res.GetLength(), 1);
	ASSERT_NE(set.Capacity(), 1);
}

TEST(BitAndOperatorTest, IncorrectBitAndOpertorRes)
{
	TritSet setA(160);
	TritSet setB(1600);

	setB[1000] = True;

	TritSet setC = setA & setB;

	ASSERT_EQ(setC.Cardinality(True), 0);
	ASSERT_EQ(setC.Cardinality(False), 0);
	ASSERT_EQ(setC.Cardinality(Unknown), 0);

	TritSet setD = setA;
	setD &= setB;

	ASSERT_EQ(setD.Cardinality(True), 0);
	ASSERT_EQ(setD.Cardinality(False), 0);
	ASSERT_EQ(setD.Cardinality(Unknown), 0);

	ASSERT_EQ(setD.GetLength(), 1);
	ASSERT_EQ(setC.GetLength(), 1);
}

TEST(BitOrOperatorTest, IncorrectBitOrOpertorRes)
{
	TritSet setA(160);
	TritSet setB(1600);
	Trit a;

	setA[11] = True;
	ASSERT_EQ(setA.Cardinality(True), 1);
	ASSERT_EQ(setA.Cardinality(False), 0);

	TritSet setC = setB | setA;

	ASSERT_EQ(setC.Cardinality(True), 1);
	ASSERT_EQ(setC.Cardinality(False), 0);

	ASSERT_EQ(setC.GetLength(), setA.GetLength());

	setB |= setA;
	a = setB[0];

	ASSERT_EQ(setB.GetLength(), 12);
	ASSERT_EQ(a, Unknown);
}

TEST(BitNotOperatorTest, IncorrectBitNotOperatorRes)
{
	TritSet setA(1600);

	setA[999] = True;
	setA[0] = True;
	Trit val = (setA[998] = True);

	TritSet setB = ~setA;

	ASSERT_EQ(setB.GetNumOfTrit(), setA.GetNumOfTrit());
	ASSERT_EQ(setB.Cardinality(False), 3);
	ASSERT_EQ(setB.Cardinality(True), 0);
}