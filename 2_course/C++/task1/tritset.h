#pragma once

#include <unordered_map>
#include <iostream>

typedef unsigned int uint;

namespace Trits
{
	enum Trit { True, False, Unknown };
	class TritSet
	{
		int size;
		int capacity;
		uint* trits;
		class TritItem
		{
			int index;
			TritSet& parent;
		public:
			TritItem(int newIndex, TritSet& newParent);
			~TritItem();

			operator Trit();
			Trit operator= (Trit newValue);
			Trit operator= (const TritItem& newItem);
			friend class TritSet;
		};
	public:
		TritSet(int newSize = 0);
		TritSet(const TritSet& origTrits);
		TritSet(TritSet&& origTrits);	//move constructor
		~TritSet();

		TritSet& operator=(const TritSet& secondTrit);
		TritSet& operator=(TritSet&& secondTrit);	//move operator=

		TritItem operator[] (const int index);

		TritSet& operator|= (const TritSet& secondTrit);

		TritSet& operator&= (const TritSet& secondTrit);

		TritSet operator~ () const;

		Trit GetTrit(int index) const;
		void SetTrit(int index, const Trit newValue);

		int GetNumOfTrit() const;
		int GetSizeOfArray() const;
		int GetLength() const;

		int Cardinality(const Trit value) const;
		std::unordered_map <Trit, int, std::hash<int>> Cardinality() const;

		void Trim(const int lastIndex);
		TritSet Shrink();
		int Capacity() const;

	};
	TritSet operator| (const TritSet& firstTrit, const TritSet& secondTrit);
	TritSet operator& (const TritSet& firstTrit, const TritSet& secondTrit);
}
std::ostream& operator<<(std::ostream& outStream, const Trits::Trit& value);