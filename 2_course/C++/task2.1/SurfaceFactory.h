#pragma once
#include "Surface.h"
#include "Cylinder.h"
#include "Tor.h"
#include "Planar.h"

class SurfaceFactory
{
	SurfaceFactory() {};
public:
	static SurfaceFactory & GetInstance()
	{
		static SurfaceFactory f;
		return f;
	}
	SpaceImple* GetSpaceImple(std::string spaceType);
};

