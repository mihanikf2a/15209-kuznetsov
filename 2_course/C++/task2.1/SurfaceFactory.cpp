#include "SurfaceFactory.h"
#include <iostream>
SpaceImple* SurfaceFactory::GetSpaceImple(std::string spaceType)
{
	if (spaceType.compare("planar")==0	)
	{
		return new Planar;
	}
	if (spaceType.compare("tor")==0)
	{
		return new Tor;
	}
	if (spaceType.compare("cylinder")==0)
	{
		return new Cylinder;
	}
	return nullptr;
}