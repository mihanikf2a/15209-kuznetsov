#include "SpaceImple.h"
#include <string>

uint SpaceImple::Move(const Point point)
{
	this->current = point;
	if (this->current.x != this->finish.x || this->current.y != this->finish.y)
	{
		this->field[this->current.y][this->current.x] = '*';
	}
	return this->DistanceToFinish(point);
}

std::istream& operator >> (std::istream& inStream, SpaceImple& spaceImple)
{
	int i = 0;
	std::string inString;
	while (!inStream.eof())
	{
		getline(inStream,inString);
		spaceImple.field.push_back(inString);
		for (uint j = 0; j < inString.length(); j++)
		{
			if (inString[j] == 'F')
			{
				spaceImple.finish = { (int)j,(int)i };
			}
			if (inString[j] == 'S')
			{
				spaceImple.start = { (int)j,(int)i };
				spaceImple.current = spaceImple.start;
			}
		}
		i++;
	}
	return inStream;
}

std::ofstream & operator<<(std::ofstream &outStream, const SpaceImple &spaceImple)
{
	for (uint i = 0; i < spaceImple.field.size(); i++)
	{
		outStream <<spaceImple.field[i] << std::endl;
	}
	return outStream;
}

void SpaceImple::SetCurrent(const char data)
{
	this->field[this->current.y][this->current.x] = data;
}

Point SpaceImple::GetCurrent() const
{
	return this->current;
}

Point SpaceImple::GetFinish() const
{
	return this->finish;
}