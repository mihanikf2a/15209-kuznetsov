#include "Cylinder.h"

uint Cylinder::DistanceToFinish(const Point fromPoint)
{
	uint distance1 = abs(fromPoint.x - this->finish.x);
	uint distance2 = this->field[fromPoint.y].size() - distance1;
	if (distance1 < distance2)
	{
		return distance1 + abs(fromPoint.y - this->finish.y);
	}
	return distance2 + abs(fromPoint.y - this->finish.y);
}

std::vector<std::tuple<Point, uint>> Cylinder::LookUp()
{
	std::vector<std::tuple<Point, uint>> res;
	int height = this->field.size();

	Point points[] = 
	{
		{ (this->current.x - 1) < 0 ? (int)this->field[this->current.y].size() - 1 : this->current.x - 1, this->current.y },		// Left Point
		{ this->current.x, this->current.y - 1 },														// Up Point
		{ (this->current.x + 1) % (int)this->field[this->current.y].size(), this->current.y },							// Right Point
		{ this->current.x, this->current.y + 1 }														// Bottom Point
	};

	for (Point p : points) 
	{
		if (p.x >= 0 && p.y >= 0 && p.y < height && p.x < (int)this->field[p.y].size())
			if (this->field[p.y][p.x] == '.' || this->field[p.y][p.x] == 'F') 
			{
				res.push_back(std::make_tuple(p, this->DistanceToFinish(p)));
			}
	}

	return res;
}