#pragma once
#include "SpaceImple.h"
class Tor :
	public SpaceImple
{
public:
	virtual std::vector <std::tuple<Point, uint>> LookUp();
	virtual uint DistanceToFinish(const Point fromPoint);
};

