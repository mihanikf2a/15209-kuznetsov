#pragma once
#include "Header.h"
class Surface
{
public:
	virtual uint Move(const Point point) = 0;
	virtual std::vector <std::tuple<Point, uint>> LookUp() = 0;
};