#include <locale>

#include "Robot.h"
#include "SurfaceFactory.h"
#include "Header.h"

int main(int argc, char** argv)
{
	setlocale(0, "Russian");
	cxxopts::Options options("MyProgram", "One line description of MyProgram");
	options.add_options()
		("h,help", "Print help")
		("s,spase", "Space name", cxxopts::value<std::string>()->default_value("space.txt"))
		("o,out", "Output file name", cxxopts::value<std::string>()->default_value("route.txt"))
		("d,dict", "Dictionary name", cxxopts::value<std::string>()->default_value("dictionary.txt"))
		("t,topology", "Topology", cxxopts::value<std::string>()->default_value("planar"))
		("l,limit", "limit value", cxxopts::value<int>())
		;
	try
	{
	options.parse(argc, argv);
	}
	catch (const cxxopts::OptionException&)
	{
		//std::cout << "error parsing options: " << e.what() << std::endl;
		callHelp();
		exit(1);
	}
	if (options.count("help"))
	{
		callHelp();
		return _HELP_CALLED_;
	}
	std::ifstream from(options["s"].as<std::string>());
	if (!from.good())
	{
		std::cout << "������ ��� �������� �����: " << "\"" << options["s"].as<std::string>() << "\"" << std::endl;
		return _INPUT_FILE_OPEN_ERROR_;
	}
	SpaceImple *space;

	space = SurfaceFactory::GetInstance().GetSpaceImple(options["t"].as<std::string>());
	if (space != nullptr)
	{
		from >> *space;
	}
	else
	{
		std::cout << "����������� �����������: " << "\"" << options["t"].as<std::string>() << "\"" << std::endl;
		return _UNKNOWN_TOPOLOGY_TYPE_;
	}
	std::ofstream to(options["o"].as<std::string>());
	if (!to.good()) 
	{
		std::cout << "������ ��� �������� �����: " << "\"" << options["o"].as<std::string>() << "\"" << std::endl;
		return _OUTPUT_FILE_OPEN_ERROR_;
	}
	Robot rbt(space);
	try 
	{
		rbt.discover();
	}
	catch (BadMove)
	{
		std::cout << "���� �� ������..." << std::endl;
		to << "���� �� ������..." << std::endl;
	}

	to << *space;
	delete space;
	return 0;
}