#include "Robot.h"

void Robot::discover()
{
	std::tuple<Point, uint> next;
	Point pNext;
	uint dist;
	do {
		pNext = this->GetCloses(space->LookUp());
		dist = space->Move(pNext);
	} while (dist);
}

Point Robot::GetCloses(const std::vector<std::tuple<Point, uint>> points)
{
	if (points.empty())
	{
		throw BadMove();
	}
	uint minDistance = std::get<1>(points[0]);
	Point soonPoint = std::get<0>(points[0]);
	for (uint i = 0; i < points.size(); ++i)
	{
		if (minDistance > std::get<1>(points[i]))
		{
			minDistance = std::get<1>(points[i]);
			soonPoint = std::get<0>(points[i]);
		}
	}
	return soonPoint;
}