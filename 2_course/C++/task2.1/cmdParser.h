#pragma once
#include <iostream>
#include <string>


class Config
{
	bool help;
	std::string space;
	std::string out;
	std::string dict;
	int limit;
	std::string topology;
public:
	Config();
	Config(int argc, char** argv);
	std::string GetSpace() const;
	bool GetHelp() const;
	std::string GetOut() const;
	std::string GetDictionary() const;
	int GetLimit() const;
	std::string GetTopology() const;

};
void callHelp();