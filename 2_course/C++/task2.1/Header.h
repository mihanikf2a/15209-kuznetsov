#pragma once
#include <vector>
#include <tuple>
#include <string>
#include "cmdParser.h"
#include "cxxopts.hpp"

const int _OUTPUT_FILE_OPEN_ERROR_ = -100;
const int _INPUT_FILE_OPEN_ERROR_ = -101;
const int _UNKNOWN_TOPOLOGY_TYPE_ = -102;
const int _BADMOVE_EXCEPTION_ = -103;
const int _GT_ALLOWABLE_LIMIT_ = -104;
const int _HELP_CALLED_ = 101;

typedef unsigned int uint;

struct Point
{
	int x;
	int y;
};
