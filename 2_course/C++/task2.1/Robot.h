#pragma once
#include "SurfaceFactory.h"
class Robot 
{
	SpaceImple *space;

public:
	Robot(SpaceImple *sp) : space(sp) {};
	void discover();
	Point GetCloses(const std::vector<std::tuple<Point, uint>> points);
};
class BadMove : public std::exception {};