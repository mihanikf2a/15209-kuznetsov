#pragma once
#include "Surface.h"
#include <fstream>

class SpaceImple :
	public Surface
{
protected:
	Point start;
	Point finish;
	Point current;
	std::vector<std::string> field;

public:
	virtual uint DistanceToFinish(const Point fromPoint)=0;
	virtual uint Move(const Point point);
	void SetCurrent(const char data);
	Point GetCurrent() const;
	Point GetFinish() const;
	friend std::istream& operator >> (std::istream&, SpaceImple&);
	friend std::ofstream& operator << (std::ofstream&, const SpaceImple&);
};

std::istream& operator >> (std::istream&, SpaceImple&);
std::ofstream& operator << (std::ofstream&, const SpaceImple&);

