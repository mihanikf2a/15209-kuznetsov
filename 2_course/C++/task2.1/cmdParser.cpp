#include "cmdParser.h"

Config::Config():help(false), space("space.txt"), out("route.txt"), dict("dictionary.txt"), limit(1000), topology("planar")
{}

Config::Config(int argc, char** argv):space("space.txt"), out("route.txt"), dict("dictionary.txt"), limit(1000), topology("planar")
{

	if (argc == 1)
	{
		this->help = true;
	}

	for (int i = 1; i < argc; ++i)
	{
		if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help"))
		{
			this->help = true;
			break;
		}

		if (!strcmp(argv[i], "-s") || !strcmp(argv[i], "--space"))
		{
			this->space = argv[++i];
			continue;
		}

		if (!strcmp(argv[i], "-o") || !strcmp(argv[i], "--out"))
		{
			this->out = argv[++i];
			continue;
		}

		if (!strcmp(argv[i], "-l") || !strcmp(argv[i], "--limit"))
		{
			this->limit = atoi(argv[++i]);
			continue;
		}

		if (!strcmp(argv[i], "-t") || !strcmp(argv[i], "--topology"))
		{
			this->topology = argv[++i];
			continue;
		}
	}
}

std::string Config::GetSpace() const
{
	return this->space;
}

bool Config::GetHelp() const
{
	return this->help;
}

std::string Config::GetOut() const
{
	return this->out;
}

std::string Config::GetDictionary() const
{
	return this->dict;
}

int Config::GetLimit() const
{
	return this->limit;
}

std::string Config::GetTopology() const
{
	return this->topology;
}

void callHelp()
{
	std::cout << "(-h --help) - ������ ��������� �� ������� ���������� ��������� � ���������� ������." << std::endl;
	std::cout << "(-s --space) - ��� ���������� ����� � ��������� ������������. (space.txt �� ���������)" << std::endl;
	std::cout << "(-o --out) - �������� ��������� ���� � ���������. (route.txt �� ���������)" << std::endl;
	std::cout << "(-l --limit) - ����� ������������ ����� ���������. ���� ������� ��������� ������ ����� �� �������������. (1000 �� ���������)" << std::endl;
	std::cout << "(-t --topology) - ������������, ������� ���������� ��������� ������������. (planar �� ���������)" << std::endl;
	std::cout << "\tplanar - ������� ����" << std::endl;
	std::cout << "\tcylinder - ���� �������� ����������� �������, ��� ���������� ������� ��� ������ ���� ����� ����� ����������� �� ������ ����� �����" << std::endl;
	std::cout << "\ttor - ���� �������� ����������� ���, ������������� � �������� �������� ������� ��� ���������� ������� � ������ ������" << std::endl;
}