package gameModel;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 16.03.2017.
 */
public abstract  class GameCell
{
    private boolean marked = false;
    private boolean opened = false;

    public abstract boolean isMine();
    public abstract boolean isEmpty();
    public abstract int getRank();

    public boolean setReverseMark()
    {
        this.marked = !this.marked;
        return this.marked;
    }

    public boolean isMarked()
    {
        return this.marked;
    }

    public boolean isOpened()
    {
        return this.opened;
    }

    public boolean setFlag()
    {
        this.marked = true;
        return  this.marked;
    }

    public boolean deleteFlag()
    {
        this.marked = false;
        return this.marked;
    }

    public boolean open()
    {
        this.opened = true;
        return this.opened;
    }
}
