package gameModel;

import gameLevel.GameLevel;

import java.util.Random;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 16.03.2017.
 */
public class GameModel
{
    private GameCell[][] gameField;
    private GameLevel level;
    private boolean firstOpen;

    public GameModel(GameLevel newLevel)
    {
        level=newLevel;
        createNewGame(level);
        firstOpen = true;
    }

    public GameLevel getLevel()
    {
        return level;
    }

    public boolean cellExists(int X, int Y)
    {
        return (X >= 0) && (X < gameField.length) && (Y >= 0) && (Y < gameField[0].length);
    }

    private void setMines()
    {
        int numMines = level.getNumMines();
        Random random = new Random();
        for(int i=0;i<numMines;++i)
        {
            boolean flag = false;
            while (!flag)
            {
                int xPos = random.nextInt(level.getHeight() - 1);
                int yPos = random.nextInt(level.getWidth()-1);
                if(gameField[xPos][yPos]==null)
                {
                    gameField[xPos][yPos] = new GameMineCell();
                    flag = true;
                }
            }
        }
    }

    private void setMines(int x , int y )
    {
        Random rand = new Random();
        for (int i = 0; i < level.getNumMines(); i++)
        {
            boolean flag = false;
            while (!flag)
            {
                int xpos = rand.nextInt(level.getHeight() - 1);
                int ypos = rand.nextInt(level.getWidth() - 1);
                if ( xpos!=x && ypos!=y ) {
                    if (null == gameField[xpos][ypos])
                    {
                        gameField[xpos][ypos] = new GameMineCell();
                        flag = true;
                    }
                }
            }
        }
    }

    private void fillRank()
    {
        int[] DX = { -1, -1, -1, 0, 0, 1, 1, 1 };
        int[] DY = { -1, 0, 1, -1, 1, -1, 0, 1 };
        for(int i=0;i<level.getHeight();++i)
        {
            for(int j=0;j<level.getWidth();++j)
            {
                if(gameField[i][j].isMine())
                {
                    for (int k=0; k<DX.length;++k)
                    {
                        if (cellExists(i + DX[k], j + DY[k]) && gameField[i + DX[k]][j + DY[k]].isEmpty())
                        {
                            GameEmptyCell curCell = (GameEmptyCell) gameField[i + DX[k]][j + DY[k]];
                            curCell.setRank(curCell.getRank() + 1);
                        }
                    }
                }
            }
        }
    }

    private void setEmptyCells()
    {
        for(int i=0;i<level.getHeight();++i)
        {
            for(int j=0;j<level.getWidth();++j)
            {
                if(gameField[i][j]==null)
                {
                    gameField[i][j] = new GameEmptyCell();
                }
            }
        }
        fillRank();
    }

    public boolean isFirstOpen()
    {
        return firstOpen;
    }

    public void setFirstOpenFalse()
    {
        firstOpen = false;
    }

    public boolean isOpened(int xPos, int yPos)
    {
        return gameField[xPos][yPos].isOpened();
    }

    public boolean isEmpty(int xPos, int yPos)
    {
        return gameField[xPos][yPos].isEmpty();
    }

    public boolean isFlagged(int xPos, int yPos)
    {
        return gameField[xPos][yPos].isMarked();
    }

    public boolean isMine(int xPos, int yPos)
    {
        return gameField[xPos][yPos].isMine();
    }

    public int getRank(int xPos, int yPos)
    {
        return gameField[xPos][yPos].getRank();
    }

    public boolean setFlag(int xPos, int yPos)
    {
        return gameField[xPos][yPos].setFlag();
    }

    public  boolean deleteFalg(int xPos, int yPos)
    {
        return gameField[xPos][yPos].deleteFlag();
    }

    public int openCell(int xPos, int yPos)
    {
        gameField[xPos][yPos].open();
        return getRank(xPos,yPos);
    }

    public void openMines()
    {
        for(int i=0;i<level.getHeight();++i)
        {
            for(int j=0;j<level.getWidth();++j)
            {
                if(gameField[i][j].isMine())
                {
                    gameField[i][j].open();
                }
            }
        }
    }

    public void createNewGame()
    {
        gameField = new GameCell[level.getHeight()][level.getWidth()];
        setMines();
        setEmptyCells();
        firstOpen = true;
    }

    public void createNewGame(GameLevel newLevel)
    {
        level = newLevel;
        createNewGame();
    }

    public void createNewGame( int x , int y )
    {
        gameField = new GameCell[level.getHeight()][level.getWidth()];
        setMines(x , y );
        setEmptyCells();
        firstOpen = true;
    }

    public void createNewGame(GameLevel newLevel , int x , int y )
    {
        this.level = newLevel;
        createNewGame( x , y );
    }
}
