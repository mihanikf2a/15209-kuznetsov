package gameModel;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 16.03.2017.
 */
public class GameEmptyCell extends GameCell
{
    private int rank = 0;

    @Override
    public boolean isEmpty()
    {
        return true;
    }

    @Override
    public boolean isMine()
    {
        return false;
    }

    public void setRank(int newRank)
    {
        rank = newRank;
    }

    @Override
    public int getRank()
    {
        return rank;
    }
}
