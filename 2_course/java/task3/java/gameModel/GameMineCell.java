package gameModel;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 16.03.2017.
 */
public class GameMineCell extends GameCell
{
    @Override
    public boolean isEmpty()
    {
        return false;
    }

    @Override
    public boolean isMine()
    {
        return true;
    }

    @Override
    public int getRank()
    {
        return 0;
    }
}
