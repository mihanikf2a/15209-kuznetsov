package gameLevel;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 16.03.2017.
 */
public interface GameLevel
{
    public int getHeight();
    public int getWidth();
    public int getNumMines();
    public String getLevelName();
}
