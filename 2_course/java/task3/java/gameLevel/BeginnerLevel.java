package gameLevel;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 16.03.2017.
 */
public class BeginnerLevel implements GameLevel
{
    private static int height = 9;
    private static int width = 9;
    private static int mines = 10;

    public int getNumMines()
    {
        return mines;
    }

    public int getHeight()
    {
        return height;
    }

    public int getWidth()
    {
        return width;
    }

    public String getLevelName()
    {
        return "Beginner";
    }
}
