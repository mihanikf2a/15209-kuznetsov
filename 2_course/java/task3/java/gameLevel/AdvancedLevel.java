package gameLevel;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 16.03.2017.
 */
public class AdvancedLevel implements GameLevel
{
    private static int mines = 90;
    private static int width = 30;
    private static int height = 16;

    public int getNumMines()
    {
        return mines;
    }

    public int getHeight()
    {
        return height;
    }

    public int getWidth()
    {
        return width;
    }

    public String getLevelName()
    {
        return "Advanced";
    }
}
