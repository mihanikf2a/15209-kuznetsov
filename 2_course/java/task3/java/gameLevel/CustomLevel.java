package gameLevel;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 16.03.2017.
 */
public class CustomLevel implements GameLevel
{
    private int mines = 0;
    private int width = 0;
    private int height = 0;

    public CustomLevel(int newWidth, int newHeight, int newNumMines)
    {
        mines = newNumMines;
        width = newWidth;
        height = newHeight;
    }

    public int getNumMines()
    {
        return mines;
    }

    public int getHeight()
    {
        return height;
    }

    public int getWidth()
    {
        return width;
    }

    public String getLevelName()
    {
        return "Custom";
    }
}
