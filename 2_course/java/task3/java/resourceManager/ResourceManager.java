package resourceManager;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 04.04.2017.
 */
public class ResourceManager
{
    //private final static String path = "resources/";
    private BufferedImage image = null;

    private Map<String, BufferedImage> imageMap = new HashMap<String, BufferedImage>();

    public BufferedImage getImage(String imageName)
    {
        if (imageName.equals(""))
        {
            return null;
        }
        if (imageMap.containsKey(imageName))
        {
            return imageMap.get(imageName);
        }
        else
        {
            try
            {
                image = ImageIO.read(ResourceManager.class.getResourceAsStream(imageName+".png"));
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
            imageMap.put(imageName, image);
            return image;
        }

    }
}