package view.gameGUI;

import controller.Controller;
import controller.GameController;
import gameLevel.GameLevel;
import table.TableOfRecords;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.logging.Level;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 03.04.2017.
 */
public class GameForm extends JFrame
{
    private Controller controller;
    private int cellSize=20;
    private JPanel panel;
    private JLabel timerLabel = new JLabel("time: 0");
    private Timer timer;
    private int seconds;
    static private TableOfRecords tableOfRecords = new TableOfRecords();

    private MouseListener mouseListener = new MouseListener() {
        public void mouseClicked(MouseEvent e)
        {
            int pozY= e.getX()/20;
            int pozX=e.getY()/20;
            if(e.getButton()==e.BUTTON1) {
                if (controller.isFirstOpen()) {
                    timer.restart();
                    if(controller.isMine(pozX,pozY))
                    {
                        controller.startNewGame(pozX,pozY);
                    }
                    controller.setFirstOpenFalse();
                }
                controller.shot(pozX,pozY);
                repaint();
            }
            if(e.getButton()==e.BUTTON3) {
                if (controller.isFlagged(pozX, pozY))
                {
                    controller.deleteFlag(pozX,pozY);
                }
                else
                {
                    controller.setFlag(pozX,pozY);
                }
                repaint();
            }

        }

        public void mousePressed(MouseEvent e) {

        }

        public void mouseReleased(MouseEvent e) {

        }

        public void mouseEntered(MouseEvent e) {

        }

        public void mouseExited(MouseEvent e) {

        }
    };

    private class TimerTick implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if(controller.isGameOver())
            {
                timer.stop();
                if(controller.isWin())
                {
                    if(tableOfRecords.getStatusRecord(seconds,controller.getLevel().getLevelName()))
                    {
                        SetNewRecordDialog snrd = new SetNewRecordDialog(seconds,controller.getLevel().getLevelName());
                        snrd.setVisible(true);
                    }
                    StartNewGame sng = new StartNewGame(GameForm.this, "You win!\n" +
                            "Time taken: " + seconds);
                    sng.setVisible(true);
                }
                else
                {
                    StartNewGame sng = new StartNewGame(GameForm.this, "You lose!\n" +
                            "Time taken: " + seconds);
                    sng.setVisible(true);
                }

            }
            else
            {
                seconds++;
                timerLabel.setText("time: "+Integer.toString(seconds));
            }
        }
    }

    private JMenu createMenuBar()
    {
        JMenu file = new JMenu("File");
        JMenuItem startNewGame = new JMenuItem("New game",null);
        JMenuItem setLevel = new JMenuItem("Set level",null);
        final JMenuItem records = new JMenuItem("Records",null);
        JMenuItem exit = new JMenuItem(new ExitAction());
        file.add(startNewGame);
        file.add(setLevel);
        file.add(records);
        file.add(exit);

        startNewGame.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent arg0)
            {
                controller.startNewGame();
                repaint();
            }
        });

        setLevel.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                SetLevelForm slf = new SetLevelForm(controller.getLevel(), GameForm.this);
                slf.setVisible(true);
            }
        });

        records.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                StatsForm sf = new StatsForm();
                sf.setVisible(true);
            }
        });
        return file;
    }

    class ExitAction extends AbstractAction
    {
        private static final long serialVersionUID = 1L;
        ExitAction()
        {
            putValue(NAME, "Exit");
        }
        public void actionPerformed(ActionEvent e)
        {
            System.exit(0);
        }
    }

    public void startNewGame()
    {
        controller.startNewGame();
        seconds = 0;
        timerLabel.setText("time: 0");
        repaint();
    }

    public void startNewGame(GameLevel level)
    {
        setBounds(100,100,level.getWidth()*cellSize+6,level.getHeight()*cellSize+52);
        controller.startNewGame(level);
        panel = new GameGraphics(controller);
        seconds = 0;
        timerLabel.setText("time: 0");
        repaint();
    }

    public GameForm(GameLevel level)
    {
        seconds = 0;
        timer = new Timer(1000, new TimerTick());
        controller = new GameController(level);
        setBounds(100,100,level.getWidth()*cellSize+6,level.getHeight()*cellSize+52);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setResizable(false);

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(createMenuBar());
        menuBar.add(timerLabel);
        setJMenuBar(menuBar);
        Container container = this.getContentPane();

        container.setLayout(new GridLayout(1,1,2,2));

        panel = new GameGraphics(controller);
        panel.setOpaque(true);

        panel.addMouseListener(mouseListener);

        container.add(panel);
    }
}
