package view.gameGUI;

import controller.Controller;
import gameLevel.GameLevel;
import resourceManager.ResourceManager;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 04.04.2017.
 */
public class GameGraphics extends JPanel
{
    private Controller controller;
    private ResourceManager resourceManager = new ResourceManager();
    public GameGraphics(Controller newController)
    {
        controller = newController;
    }

    public void paint(Graphics g)
    {
        BufferedImage image;
        for(int i=0;i<controller.getLevel().getHeight();++i)
        {
            for(int j=0;j<controller.getLevel().getWidth();++j)
            {
                if(controller.isFlagged(i,j))
                {
                    image = resourceManager.getImage("flag");
                }
                else if(!controller.isOpened(i,j))
                {
                    image = resourceManager.getImage("slot");
                }
                else if(controller.isMine(i,j))
                {
                    image = resourceManager.getImage("mine");
                }
                else
                {
                    int rank = controller.getRank(i,j);
                    image = resourceManager.getImage(Integer.toString(rank));
                }
                g.drawImage(image,20*j,20*i,null);
            }
        }
    }
}
