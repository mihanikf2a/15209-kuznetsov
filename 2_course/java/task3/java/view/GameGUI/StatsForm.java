package view.gameGUI;

import table.TableOfRecords;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class StatsForm extends JDialog {
    static private TableOfRecords records = new TableOfRecords();

    private JPanel contentPane;
    private JButton buttonCancel;
    private JPanel tableContainer;

    private JTable createTable()
    {
        String[] columnNames = {"Level","Name","Time"};
        JTable table = new JTable(records.getTable(),columnNames);
        table.getColumnModel().getColumn(0).setPreferredWidth(200);
        table.getColumnModel().getColumn(1).setPreferredWidth(150);
        table.getColumnModel().getColumn(2).setPreferredWidth(100);

        for(int i=0;i<3;++i)
        {
            table.setRowHeight(i,20);
        }

        return table;
    }

    public StatsForm() {
        setContentPane(contentPane);
        setModal(true);
        setBounds(100,100,250,150);
        setResizable(false);
        tableContainer.setLayout(new GridLayout(1,1,2,2));
        JTable table = createTable();
        table.setBackground(getBackground());
        tableContainer.add(table);

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }
}
