package view.gameGUI;

import gameLevel.CustomLevel;
import gameLevel.GameLevel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 03.04.2017.
 */
public class CustomLevelForm extends JFrame {
    private JButton buttonCancel = new JButton("Cancel");
    private JButton buttonStart = new JButton("Start");
    private JLabel heightLabel = new JLabel("Height");
    private JLabel widthLabel = new JLabel("Width");
    private JLabel minesLabel = new JLabel("Mines");
    private JTextField heightField = new JTextField();
    private JTextField widthField = new JTextField();
    private JTextField minesField = new JTextField();

    private GameLevel level;
    private GameForm gameForm;

    public CustomLevelForm(GameLevel oldLevel,GameForm nGameForm)
    {
        gameForm = nGameForm;
        level = oldLevel;
        setBounds(100, 100, 200, 180);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = this.getContentPane();
        container.setLayout(new GridLayout(4, 2, 2, 2));

        heightField.setText("" + level.getHeight());
        widthField.setText("" + level.getWidth());
        minesField.setText("" + level.getNumMines());

        container.add(heightLabel);
        container.add(heightField);
        container.add(widthLabel);
        container.add(widthField);
        container.add(minesLabel);
        container.add(minesField);
        container.add(buttonStart);
        container.add(buttonCancel);

        buttonCancel.addActionListener(new ButtonCancelEventListener());
        buttonStart.addActionListener(new ButtonStartEventListener());
    }

    private class ButtonCancelEventListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            SetLevelForm slf = new SetLevelForm(level,gameForm);
            slf.setVisible(true);
            dispose();
        }
    }
    private class ButtonStartEventListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            Integer height;
            Integer width;
            Integer mines;
            try
            {
                height = Integer.parseInt(heightField.getText());
                width = Integer.parseInt(widthField.getText());
                mines = Integer.parseInt(minesField.getText());
            }
            catch (NumberFormatException ex)
            {
                JOptionPane.showMessageDialog(null,
                        "Incorrect input data",
                        "Error",
                        JOptionPane.PLAIN_MESSAGE);
                return;
            }
            gameForm.startNewGame(new CustomLevel(width,height,mines));
            dispose();
        }
    }
}
