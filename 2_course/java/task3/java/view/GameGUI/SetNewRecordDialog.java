package view.gameGUI;

import table.TableOfRecords;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class SetNewRecordDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JPanel panel;
    private JTextPane textPane = new JTextPane();
    private JLabel label = new JLabel();
    private int time;
    private String level;
    private TableOfRecords tableOfRecords= new TableOfRecords();

    public SetNewRecordDialog(int newTime,String newLevel) {
        time = newTime;
        level = newLevel;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setResizable(false);
        setBounds(100,100,300,125);
        panel.setLayout(new GridLayout(2,1,2,2));
        label.setText("You have set a new record!\n Enter you name.");
        panel.add(label);
        panel.add(textPane);
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK()
    {
        String name;
        name = textPane.getText();
        if (name.length()==0)
       {
           JOptionPane.showMessageDialog(null,
            "You don't enter your name",
            "Error",
            JOptionPane.PLAIN_MESSAGE);
        }
        else
        {
            tableOfRecords.setNewRecord(time,name,level);
            dispose();
        }
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }
}
