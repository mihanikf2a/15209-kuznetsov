package view.gameGUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class StartNewGame extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JPanel container;
    private GameForm gameForm;
    private JTextPane label;


    public StartNewGame(GameForm gf, String message) {
        setTitle("GAME OVER!");

        label = new JTextPane();
        label.setEditable(false);
        Color color = this.getBackground();
        label.setBackground(color);
        label.setText(message + "\nStart new game?");

        setContentPane(contentPane);
        container.setLayout(new GridLayout(1,1,2,2));
        container.add(label);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        gameForm = gf;

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        setBounds(100,100,200,150);
        setResizable(false);

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        gameForm.startNewGame();
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        System.exit(0);
        dispose();
    }
}
