package view.gameGUI;

import gameLevel.BeginnerLevel;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 03.04.2017.
 */
public class MainGUI
{
    public static void main(String argv[])
    {
        GameForm form = new GameForm(new BeginnerLevel());
        form.setVisible(true);
    }
}
