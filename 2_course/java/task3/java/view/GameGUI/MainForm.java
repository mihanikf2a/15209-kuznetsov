package view.gameGUI;

import gameLevel.BeginnerLevel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 03.04.2017.
 */
public class MainForm extends JFrame
{
    private JButton buttonStart = new JButton("Start");
    private JButton buttonRecords = new JButton("Records");
    private JButton buttonLevel = new JButton("Level");
    private JButton buttonExit = new JButton("Exit");
    public MainForm()
    {
        setBounds(100,100,100,200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = this.getContentPane();
        container.setLayout(new GridLayout(4,1,2,2));

        buttonStart.addActionListener(new ButtonStartEventListener());
        buttonLevel.addActionListener(new ButtonLevelEventListener());
        buttonRecords.addActionListener(new ButtonStatsEventListener());
        buttonExit.addActionListener(new ButtonExitEventListener());

        container.add(buttonStart);
        container.add(buttonLevel);
        container.add(buttonRecords);
        container.add(buttonExit);
    }

    private class ButtonStartEventListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            GameForm gm = new GameForm(new BeginnerLevel());
            gm.setVisible(true);
            dispose();
        }
    }

    private class ButtonStatsEventListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            StatsForm statsForm = new StatsForm();
            statsForm.setVisible(true);
        }
    }

    private class ButtonLevelEventListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            SetLevelForm setLevel = new SetLevelForm(new BeginnerLevel());
            setLevel.setVisible(true);
            dispose();
        }
    }

    private class ButtonExitEventListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            dispose();
        }
    }
}

