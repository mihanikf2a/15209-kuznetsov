package view.gameGUI;

import gameLevel.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 03.04.2017.
 */
public class SetLevelForm extends JFrame
{
    private JButton buttonExit = new JButton("Cancel");
    private JButton buttonStart = new JButton("Start");
    private JRadioButton easyButton = new JRadioButton("Beginner");
    private JRadioButton advancedButton = new JRadioButton("Advanced");
    private JRadioButton interButton = new JRadioButton("Intermediate");
    private JRadioButton customButton = new JRadioButton("Custom");
    private GameForm gameForm;
    private GameLevel level;

    public SetLevelForm(GameLevel oldLevel, GameForm nGameForm)
    {
        setBounds(100,100,200,200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        gameForm = nGameForm;

        level = oldLevel;

        Container container = this.getContentPane();
        Container containerButtons = new Container();
        container.setLayout(new GridLayout(5,1,2,2));
        containerButtons.setLayout(new GridLayout(1,2,2,2));

        ButtonGroup levelGroup = new ButtonGroup();

        if (level.getClass() == IntermediateLevel.class)
        {
            interButton.setSelected(true);
        }

        if(level.getClass()== BeginnerLevel.class)
        {
            easyButton.setSelected(true);
        }
        if(level.getClass()== AdvancedLevel.class)
        {
            advancedButton.setSelected(true);
        }

        if(level.getClass()== CustomLevel.class)
        {
            customButton.setSelected(true);
        }

        levelGroup.add(easyButton);
        levelGroup.add(advancedButton);
        levelGroup.add(interButton);
        levelGroup.add(customButton);

        buttonExit.addActionListener(new ButtonExit1EventListener());
        buttonStart.addActionListener(new ButtonStartEventListener());

        container.add(easyButton);
        container.add(interButton);
        container.add(advancedButton);
        container.add(customButton);
        containerButtons.add(buttonStart);
        containerButtons.add(buttonExit);
        container.add(containerButtons);
    }
    private class ButtonExit1EventListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            dispose();
        }
    }

    private class ButtonStartEventListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if (easyButton.isSelected())
            {
                level = new BeginnerLevel();
            }
            if(interButton.isSelected())
            {
                level = new IntermediateLevel();
            }
            if(advancedButton.isSelected())
            {
                level = new AdvancedLevel();
            }
            if(customButton.isSelected())
            {
                CustomLevelForm clf = new CustomLevelForm(level,gameForm);
                clf.setVisible(true);
                dispose();
                return;
            }
            gameForm.startNewGame(level);
            dispose();
        }
    }
}
