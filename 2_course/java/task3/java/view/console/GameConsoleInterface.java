package view.console;

import controller.Controller;
import controller.GameController;
import gameLevel.*;
import gameModel.GameModel;

import java.util.Scanner;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 30.03.2017.
 */
public class GameConsoleInterface
{
    private Controller controller;
    private Scanner scanner = new Scanner(System.in);
    private GameLevel level;

    public GameConsoleInterface()
    {
        System.out.println("option game level: ");
        System.out.println("b - Beginner; i - intermediate; a - advanced; c - custom");
        System.out.println("select the level game: ");
        String descriptor = scanner.next();
        if(descriptor.equals("c"))
        {
            System.out.println("enter size field and number of mine (width, height, number of mines): ");
            level = new CustomLevel(scanner.nextInt(),scanner.nextInt(),scanner.nextInt());
        }
        else if(descriptor.equals("b"))
        {
            level = new BeginnerLevel();
        }
        else if(descriptor.equals("a"))
        {
            level = new AdvancedLevel();
        }
        else if (descriptor.equals("i"))
        {
            level = new IntermediateLevel();
        }
        else
        {
            System.out.println("Bad input!");
        }
        controller = new GameController(level);
    }

    public void start()
    {
        printGameMap();
        printGuide();
        printField();
        for(;;) 
        {
            String descriptor = scanner.next();
            if(descriptor.equals("n"))
            {
                controller.startNewGame();
            }
            else 
            {
                if(descriptor.equals("o")||descriptor.equals("s")||descriptor.equals("d"))
                {
                    System.out.println("enter position (x,y): ");
                    int y = scanner.nextInt();
                    int x = scanner.nextInt();
                    if (descriptor.equals("o") && controller.isFirstOpen()) {
                        controller.setFirstOpenFalse();
                        if (controller.isMine(x, y)) {
                            controller.startNewGame(x, y);
                            printGameMap();
                        }
                        controller.shot(x, y);
                    } else if (descriptor.equals("o")) {
                        controller.shot(x, y);
                    } else if (descriptor.equals("s")) {
                        controller.setFlag(x, y);
                    } else if (descriptor.equals("d")) {
                        controller.deleteFlag(x, y);
                    }
                }
                else
                {
                    System.out.println("incorrect values");
                }
            }
            printField();
            if(controller.isGameOver())
            {
                if(controller.isLose())
                {
                    System.out.println("You lose!");
                }
                else
                {
                    System.out.println("You win!");
                }
                System.out.println("New game? (y/n)");
                descriptor = scanner.next();
                if (descriptor.toUpperCase().equals("Y"))
                {
                    controller.startNewGame();
                    printGameMap();
                    printField();
                }
                else
                {
                    System.exit(0);
                }
            }
        }
    }

    private void printGuide()
    {
        System.out.println();
        System.out.println("o for Open cell");
        System.out.println("s for Set flag");
        System.out.println("d for Delete flag");
        System.out.println("n for start New game");
        System.out.println();
    }

    private void printGameMap()
    {
        System.out.println("game map: ");
        for (int i = 0; i < level.getHeight(); ++i)
        {
            for (int j = 0; j < level.getWidth(); ++j)
            {
                String str = "";
                if (controller.isEmpty(i, j))
                {
                    str = "e";
                }
                else if (controller.isMine(i, j))
                {
                    str = "m";
                }
                else
                {
                    str = "u";
                }
                System.out.print(str + " ");
            }
            System.out.println();
        }
    }

    private void printField()
    {
        System.out.println("current map: ");
        int heightField = level.getHeight();
        int widthField = level.getWidth();

        for (int i = 0; i < heightField; i++)
        {
            for (int j = 0; j < widthField; j++)
            {
                String str = "";
                if (controller.isFlagged(i, j))
                {
                    str = "f";
                }
                else if (!controller.isOpened(i, j))
                {
                    str = "o";
                }
                else if (controller.isMine(i, j))
                {
                    str = "m";
                }
                else
                {
                    int rank = controller.getRank(i, j);
                    if (0 == rank)
                    {
                        str = "c";
                    }
                    else
                    {
                        str = "" + rank;
                    }
                }
                System.out.print(str + " ");
            }
            System.out.println();
        }
    }
}
