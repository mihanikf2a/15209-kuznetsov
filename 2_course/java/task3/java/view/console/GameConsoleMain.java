package view.console;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 03.04.2017.
 */
public class GameConsoleMain
{
    public static void main(String argv[])
    {
        GameConsoleInterface game = new GameConsoleInterface();
        game.start();
    }
}
