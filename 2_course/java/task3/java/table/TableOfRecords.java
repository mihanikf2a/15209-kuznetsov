package table;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 17.03.2017.
 */

public class TableOfRecords
{
    private List<Record> records = new LinkedList<Record>();

    private List<String> parseRecord(String line)
    {
        List<String> rezultList = new ArrayList<String>();
        int i=0,j=0;
        while ((i = line.indexOf(";",i))>0)
        {
            rezultList.add(line.substring(j,i));
            j = ++i;
        }
        rezultList.add(line.substring(j,line.length()));
        return rezultList;
    }

    public TableOfRecords()
    {
        String line;
        BufferedReader bufferedStream;
        try
        {
            bufferedStream = new BufferedReader(new InputStreamReader(new FileInputStream("RecordsTable.csv")));
            for(int i=0;i<3;++i)
            {
                line = bufferedStream.readLine();
                records.add(new Record(parseRecord(line)));
            }
            bufferedStream.close();
        }
        catch (FileNotFoundException ex)
        {
            ex.printStackTrace();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }

    private void printRecord()
    {
        BufferedWriter bufferedStream;
        try
        {
            bufferedStream = new BufferedWriter( new OutputStreamWriter(new FileOutputStream("RecordsTable.csv")));
            for(int i=0;i<3;++i)
            {
                String line = records.get(i).getLevelName()+";"+
                        records.get(i).getName()+";"+
                        Integer.toString(records.get(i).getTime())+";\n";
                bufferedStream.write(line);
            }
            bufferedStream.close();
        }
        catch (FileNotFoundException ex)
        {
            ex.printStackTrace();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }

    public boolean getStatusRecord(int time, String level)
    {
        for(int i=0;i<3;++i)
        {
            if(records.get(i).getLevelName().equals(level))
            {
                if(records.get(i).getTime()>time)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        return false;
    }

    public void setNewRecord(int time, String name, String level)
    {
        for(int i=0;i<3;++i)
        {
            if(records.get(i).getLevelName().equals(level))
            {
                records.get(i).setNewRecord(name,time);
            }
        }
        printRecord();
    }

    public Object[][] getTable()
    {
        Object[][] table = new Object[3][3];
        Iterator<Record> iterator = records.iterator();
        for(int i=0;i<3;i++)
        {
            Record tempRecord = iterator.next();
            table[i][0] = tempRecord.getLevelName();
            table[i][1] = tempRecord.getName();
            table[i][2] = tempRecord.getTime();
        }
        return table;
    }
}