package table;

import gameLevel.GameLevel;

import java.util.List;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 06.04.2017.
 */
public class Record
{
        private String level;
        private String name;
        private int time;

        public Record(List<String> listRecord)
        {
            level = listRecord.get(0);
            name = listRecord.get(1);
            try
            {
                time = Integer.parseInt(listRecord.get(2));
            }
            catch (NumberFormatException ex)
            {
                ex.printStackTrace();
            }
        }

        public int getTime()
        {
            return time;
        }

        public String getLevelName()
        {
            return level;
        }

        public String getName()
        {
            return name;
        }

        public void setNewRecord(String newName, int newTime)
        {
            time = newTime;
            name = newName;
        }
}
