package controller;

import gameLevel.GameLevel;
import gameModel.GameModel;

import java.util.LinkedList;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 16.03.2017.
 */
public class GameController implements Controller
{
    static private int[] DX = { -1, -1, -1, 0, 0, 1, 1, 1 };
    static private int[] DY = { -1, 0, 1, -1, 1, -1, 0, 1 };
    private int openedCells = 0;
    private boolean win = false;
    private boolean lose = false;
    private boolean gameOver = false;
    private GameModel model = null;

    private void setWin()
    {
        gameOver = true;
        lose = false;
        win = true;
    }

    private void setLose()
    {
        gameOver = true;
        lose = true;
        win = false;
    }

    private void openCell(int posX, int posY)
    {
        openedCells++;
        model.openCell(posX, posY);
        if(model.isEmpty(posX, posY))
        {
            GameLevel level = model.getLevel();
            if((level.getHeight() * level.getWidth())-level.getNumMines()==openedCells)
            {
                setWin();
                return;
            }
        }
        else
        {
            setLose();
            return;
        }
    }

    private void openCircumference(int posX, int posY)
    {
        GameLevel level = model.getLevel();
        if (!model.cellExists(posX, posY) || model.isOpened(posX, posY) || model.isFlagged(posX, posY))
        {
            return;
        }
        if(model.isMine(posX,posY))
        {
            model.openMines();
            setLose();
            return;
        }
        openCell(posX,posY);
        LinkedList<Integer> queue = new LinkedList<Integer>();
        if(model.getRank(posX, posY)==0)
        {
            queue.addLast(new Integer( posX* level.getWidth()+posY));
        }
        while (!queue.isEmpty())
        {
            int curX = queue.getLast();
            queue.removeLast();
            int curY = curX%level.getWidth();
            curX /= level.getWidth();
            for(int i=0;i<DX.length;++i)
            {
                if(!model.cellExists(curX+DX[i], curY+DY[i])||model.isOpened(curX+DX[i], curY+DY[i])
                        ||model.isFlagged(curX+DX[i], curY+DY[i]))
                {
                    continue;
                }
                openCell(curX+DX[i], curY+DY[i]);
                if(model.getRank(curX+DX[i], curY+DY[i])==0)
                {
                    queue.addLast(new Integer((curX + DX[i]) * level.getWidth() + curY + DY[i]));
                }
            }
        }
    }

    private void cleanUp()
    {
        win = false;
        lose = false;
        gameOver = false;
        openedCells = 0;
    }

    public GameController(GameLevel level)
    {
        model = new GameModel(level);
    }

    public boolean shot(int posX, int posY)
    {
        if(!model.cellExists(posX,posY))
        {
            return true;
        }
        if(!model.isOpened(posX,posY))
        {
            openCircumference(posX, posY);
        }
        return isGameOver();
    }

    public void setFlag(int posX, int posY)
    {
        if(model.cellExists(posX,posY)&&!model.isOpened(posX, posY))
        {
            model.setFlag(posX, posY);
        }
    }

    public void deleteFlag(int posX, int posY)
    {
        if(model.cellExists(posX, posY))
        {
            model.deleteFalg(posX, posY);
        }
    }

    public boolean isWin()
    {
        return win;
    }

    public boolean isLose()
    {
        return lose;
    }

    public boolean isGameOver()
    {
        return gameOver;
    }

    public void startNewGame()
    {
        cleanUp();
        model.createNewGame();
    }

    public void startNewGame(GameLevel newLevel)
    {
        cleanUp();
        model.createNewGame(newLevel);
    }
    public void startNewGame( int x , int y )
    {
        cleanUp();
        model.createNewGame(x,y);
    }

    public void startNewGame(GameLevel newLevel , int x , int y )
    {
        cleanUp();
        model.createNewGame(newLevel, x,y);
    }

    public boolean isOpened(int xPos, int yPos)
    {
        return model.isOpened(xPos,yPos);
    }

    public boolean isEmpty(int xPos, int yPos)
    {
        return model.isEmpty(xPos, yPos);
    }

    public boolean isFlagged(int xPos, int yPos)
    {
        return model.isFlagged(xPos,yPos);
    }

    public boolean isMine(int xPos, int yPos)
    {
        return model.isMine(xPos, yPos);
    }

    public int getRank(int xPos, int yPos)
    {
        return model.getRank(xPos, yPos);
    }

    public GameLevel getLevel()
    {
        return model.getLevel();
    }

    public boolean isFirstOpen()
    {
        return model.isFirstOpen();
    }

    public void setFirstOpenFalse()
    {
        model.setFirstOpenFalse();
    }

}
