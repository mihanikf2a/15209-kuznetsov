package controller;

import gameLevel.GameLevel;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 16.03.2017.
 */
public interface Controller
{
    public boolean shot(int posX, int posY);

    public void setFlag(int posX, int posY);

    public void deleteFlag(int posX, int posY);

    public boolean isWin();

    public boolean isLose();

    public boolean isGameOver();

    public void startNewGame();

    public void startNewGame(GameLevel newLevel);

    public void startNewGame( int x , int y );

    public void startNewGame(GameLevel newLevel , int x , int y );

    public boolean isOpened(int xPos, int yPos);

    public boolean isEmpty(int xPos, int yPos);

    public boolean isFlagged(int xPos, int yPos);

    public boolean isMine(int xPos, int yPos);

    public int getRank(int xPos, int yPos);

    public GameLevel getLevel();

    public boolean isFirstOpen();

    public void setFirstOpenFalse();
}
