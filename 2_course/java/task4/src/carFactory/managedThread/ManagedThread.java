package carFactory.managedThread;

import static carFactory.managedThread.ThreadState.*;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 05.05.2017.
 */

public class ManagedThread extends Thread
{
    private ThreadState desiredState = RUNNING;
    private Object monitor = new Object();

    protected synchronized ThreadState getDesiredState() {return desiredState;}

    public void mstop () {
        synchronized (monitor) {
            desiredState = STOP;
            monitor.notifyAll(); // required to stop sleeping threads
        }
    }

    public void msuspend() {
        synchronized (monitor) {
            // Thread intended to stop can not be suspended
            if (desiredState != STOP)
                desiredState = SLEEP;
        }
    }

    public void mresume ()
    {
        synchronized (monitor) {
            //Only sleeping thread can be resumed
            if (desiredState == SLEEP)
            {
                desiredState = RUNNING;
                monitor.notifyAll();
            }
        }
    }

    protected boolean keepRunning()
    {
        synchronized (monitor) {
            if (desiredState == RUNNING)
                return true;
            else {
                while (true) {
                    if (desiredState == STOP)
                    {
                        System.out.println(Thread.currentThread().getName()+ " stopped");
                        return false;
                    }
                    else if (desiredState == SLEEP)
                        try {
                            System.out.println(Thread.currentThread().getName()+ " fell asleep");
                            monitor.wait();
                        } catch (InterruptedException ex) {
                            System.err.println(Thread.currentThread().getName()+" interrupted");
                            return false;
                        }
                    else if (desiredState == RUNNING) {
                        System.out.println(Thread.currentThread().getName()+" resumed");
                        return true;
                    }
                }
            }
        }
    }
}
