package carFactory;

import carFactory.managedThread.ManagedThread;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 04.05.2017.
 */
public class Dealer extends ManagedThread
{
    private long timeout;
    private Storage storage;
    private ControllerBuild controllerBuild;

    public Dealer(long timeout, ControllerBuild controllerBuild, Storage storage)
    {
        super();
        this.timeout = timeout;
        this.controllerBuild = controllerBuild;
        this.storage = storage;
    }

    public void run()
    {
        while (true)
        {
            try
            {
                Thread.sleep(timeout);
                controllerBuild.makeNewCar();
                storage.get();
                System.out.println("got car");
                if(!keepRunning())
                {
                    break;
                }
            }
            catch (InterruptedException ex)
            {
                System.err.println("Thread was inetrrupted:"+getName());
                break;
            }
        }
    }
}
