package carFactory;

import carFactory.threadPool.ThreadPool;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 04.05.2017.
 */
public class ControllerBuild
{
    private ThreadPool threadPool;

    public ControllerBuild(ThreadPool threadPool)
    {
        this.threadPool = threadPool;
    }

    public void makeNewCar()
    {
        threadPool.addTask(new MakeNewCar(threadPool.getStorages(), threadPool.getTimeout()));
    }

    public void mstop()
    {
        threadPool.mstop();
    }
}
