package carFactory.threadPool;

import carFactory.Storage;
import carFactory.managedThread.ManagedThread;

import java.util.*;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 04.05.2017.
 */
public class ThreadPool {
    private int buildersCount;
    private List taskQueue = new LinkedList();
    private Set availableThreads = new HashSet();
    private Storage[] storages;
    private int timeout;

    public void addTask(Task task) {
        synchronized (taskQueue) {
            taskQueue.add(new ThreadPoolTask(task));
            taskQueue.notifyAll();
        }
    }


    public ThreadPool(Storage[] storages, int count, int timeout) {
        this.storages = storages;
        this.buildersCount = count;
        this.timeout = timeout;
        for (int i = 0; i < buildersCount; ++i) {
            availableThreads.add(new PooledThread(taskQueue));
        }
        for (Iterator iter = availableThreads.iterator(); iter.hasNext(); ) {
            ((ManagedThread) iter.next()).start();
        }
    }

    public void mstop() {
        synchronized (taskQueue) {
            for (Iterator iter = availableThreads.iterator(); iter.hasNext(); ) {
                ManagedThread mt = (ManagedThread) iter.next();
                mt.mstop();
                taskQueue.notifyAll();
            }
        }
    }

    public void mjoin() {
        for (Iterator iter = availableThreads.iterator(); iter.hasNext(); ) {
            try {
                ((ManagedThread) iter.next()).join();
            } catch (InterruptedException ex) {
            }
        }
    }

    public Storage[] getStorages()
    {
        return storages;
    }

    public int getTimeout()
    {
        return timeout;
    }

}
