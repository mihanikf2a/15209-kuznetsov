package carFactory.threadPool;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 12.05.2017.
 */
public interface Task {
    public String getName();
    public void performWork() throws InterruptedException;
}
