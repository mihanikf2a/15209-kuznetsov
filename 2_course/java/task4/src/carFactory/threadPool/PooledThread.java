package carFactory.threadPool;

import carFactory.managedThread.ManagedThread;
import carFactory.threadPool.ThreadPoolTask;

import java.util.List;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 04.05.2017.
 */
public class PooledThread extends ManagedThread
{
    private List taskQueue;

    public PooledThread(List taskQueue)
    {
        super();
        this.taskQueue = taskQueue;
    }

    private void performTask(ThreadPoolTask threadPoolTask)
    {

        try {
            threadPoolTask.go();
        }
        catch (InterruptedException ex)
        {}
    }

    public void run()
    {
        ThreadPoolTask toExecute = null;
        while (true)
        {
            synchronized (taskQueue)
            {
                if(!keepRunning())
                {
                    break;
                }
                if (taskQueue.isEmpty())
                {
                    try {
                        taskQueue.wait();
                    }
                    catch (InterruptedException ex) {
                        System.err.println("Thread was inetrrupted:"+getName());
                        break;
                    }
                    continue;
                }
                else
                {
                    toExecute = (ThreadPoolTask) taskQueue.remove(0);
                    performTask(toExecute);
                }
            }
        }
    }
}
