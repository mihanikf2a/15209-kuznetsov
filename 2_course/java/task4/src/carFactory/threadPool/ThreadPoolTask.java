package carFactory.threadPool;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 12.05.2017.
 */
public class ThreadPoolTask {
    private Task task;
    public ThreadPoolTask(Task task)
    {
         this.task = task;
    }
    public void go() throws InterruptedException
    {
        task.performWork();
    }
    String getName()
    {
     return task.getName();
    }
}
