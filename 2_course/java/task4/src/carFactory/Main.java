package carFactory;

import carFactory.threadPool.ThreadPool;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 04.05.2017.
 */
public class Main
{
    public static void main(String[] args)
    {
        Storage[] storages = new Storage[4];
        for(int i=0;i<4;++i)
        {
            storages[i] = new Storage();
        }
        Supplier[] suppliers = new Supplier[7];
        suppliers[0] = new Supplier(storages[0],5000);
        suppliers[1] = new Supplier(storages[1],1000);
        suppliers[0].start();
        suppliers[1].start();
        for(int i=2;i<7;++i)
        {
            suppliers[i] = new Supplier(storages[2],2000);
            suppliers[i].start();
        }
        ThreadPool threadPool = new ThreadPool(storages,5,800);
        ControllerBuild controllerBuild = new ControllerBuild(threadPool);
        Dealer[] dealers = new Dealer[8];
        for(int i=0;i<8;++i)
        {
            dealers[i] = new Dealer(i*2000,controllerBuild,storages[3]);
            dealers[i].start();
        }
        try {
            Thread.sleep(6000);
        }
        catch (InterruptedException ex)
        {
        }
        for(int i=0;i<8;++i)
        {
            dealers[i].mstop();
        }
        for(int i=0;i<8;++i)
        {
            try
            {
                dealers[i].join();
            }
            catch (InterruptedException ex)
            {
            }
        }
        controllerBuild.mstop();
        for(int i=0;i<7;++i)
        {
            suppliers[i].mstop();
        }
    }
}
