package carFactory;

import carFactory.managedThread.ManagedThread;
/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 04.05.2017.
 */
public class Supplier extends ManagedThread
{
    private Storage storage;
    private long timeout;
    public Supplier(Storage storage, long delay)
    {
        super();
        this.storage = storage;
        this.timeout = delay;
    }

    public void run()
    {
        while (true)
        {
            try {
                Thread.sleep(timeout);
                storage.put("Item");     //CRITICAL POINT
                //System.out.println("put item");
                if (!keepRunning())
                {
                    break;
                }
            } catch (InterruptedException ex) {
                System.out.println(Thread.currentThread().getName()+" interrupted");
                break;
            }
        }
    }
}
