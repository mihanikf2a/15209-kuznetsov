package carFactory;

import carFactory.threadPool.Task;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 12.05.2017.
 */
public class MakeNewCar implements Task
{
    private Storage accs;
    private Storage bodys;
    private Storage engines;
    private Storage mainStorage;
    private long timeout;


    public String getName()
    {
        return "name";
    }
    public MakeNewCar(Storage[] storages, int delay)
    {
        engines = storages[0];
        bodys = storages[1];
        accs = storages[2];
        mainStorage = storages[3];
        timeout = delay;
    }

    public void performWork()
    {
        engines.get();
        System.out.println("got engine");
        bodys.get();
        System.out.println("got body");
        accs.get(5);
        System.out.println("got acc");
        try
        {
            Thread.sleep(timeout);
        }
        catch (InterruptedException ex)
        {

        }
        mainStorage.put(new Object());
        System.out.println("put car");
    }
}
