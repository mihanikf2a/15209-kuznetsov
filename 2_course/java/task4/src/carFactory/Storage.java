package carFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 04.05.2017.
 */
public class Storage
{
    private List items = new LinkedList();

    synchronized void put(Object obj)
    {
        items.add(obj);
        notifyAll ();
    }

    synchronized Object get()
    {
        while (true)
        {
            if (!items.isEmpty()) {
                return items.remove(0);
            }
            else {
                try {
                    wait();
                } catch (InterruptedException ex) {
                    System.out.println("Interrupted in wait");
                    continue;
                }
            }
        }
    }

    synchronized Object get(int n)
    {
        while (true)
        {
            if (items.size()> n)
            {
                for(int i=0;i<n-1;++i)
                {
                    items.remove(0);
                }
                return items.remove(0);
            }
            else {
                try {
                    wait();
                } catch (InterruptedException ex) {
                    System.out.println("Interrupted in wait");
                    continue;
                }
            }
        }
    }

    public boolean isEmpty()
    {
        return items.isEmpty();
    }
}
