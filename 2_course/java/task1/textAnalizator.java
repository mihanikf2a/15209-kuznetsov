package task1;

import java.util.*;
import java.io.*;

/**
 * Created by Mike Kuznetsov mihlmih@mail.ru on 02.03.2017.
 */

class Word implements Comparable<Word>
{
    String word;
    int count;

    public int compareTo(Word toWord)
    {
        return this.count-toWord.count;
    }
}

class BulderRezult
{
    HashMap<String, Integer> mapWords;
    int countWords;
}

public class textAnalizator
{
    public  static  void main(String argv[])
    {
        if(argv.length==1)
        {
            BulderRezult rezult = new BulderRezult();
            rezult.mapWords = new HashMap<String, Integer>();
            rezult.countWords=0;
            try(BufferedReader br = new BufferedReader (new FileReader(argv[0])))
            {
                int readedSymbol;
                char simbol;
                StringBuilder newString = new StringBuilder();
                while((readedSymbol=br.read())!=-1)
                {
                    simbol = (char)readedSymbol;
                    if(Character.isLetterOrDigit(simbol))
                    {
                        newString.append(simbol);
                    }
                    else
                    {
                        if(newString.length()>0)
                        {
                            if(rezult.mapWords.containsKey(newString.toString()))
                            {
                                rezult.mapWords.put(newString.toString(),rezult.mapWords.get(newString.toString())+1);
                            }
                            else
                            {
                                rezult.mapWords.put(newString.toString(),1);
                            }
                            ++rezult.countWords;
                            newString.delete(0, newString.length());
                        }
                    }
                }
                br.close();
            }
            catch (IOException ex)
            {
                System.out.println(ex.getMessage());
            }

            List<Word> list = new ArrayList<Word>();
            for( Map.Entry<String,Integer> entry : rezult.mapWords.entrySet()){
                Word wordToPaste = new Word();
                wordToPaste.count = entry.getValue();
                wordToPaste.word = entry.getKey();
                list.add(wordToPaste);
            }
            Collections.sort(list);
            Writer fileToWrite = null;
            try
            {
                fileToWrite = new OutputStreamWriter(new FileOutputStream("output.csv"));
            }
            catch(IOException e)
            {
                System.err.println("Error while opening write file:"+ e.getLocalizedMessage());
            }
            for(int index = 0; index < list.size(); index++)
            {
                double frequency = ((double) list.get(index).count / rezult.countWords)*100;
                String str = new String();
                try
                {
                    fileToWrite.write((list.get(index)).word+";");
                    fileToWrite.write(list.get(index).count + ";");
                    fileToWrite.write(frequency+"%\n");
                }
                catch(IOException e)
                {
                    System.err.println("Error until writing" + e.getLocalizedMessage());
                }
            }
            try
            {
                fileToWrite.close();
            }
            catch (IOException ex)
            {
                System.err.println("Error while closing writed file" + ex.getLocalizedMessage());
            }
        }
        else
        {
            System.out.println("Incorrect number of arguments");
        }
    }
}
