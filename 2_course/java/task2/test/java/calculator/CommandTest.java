package calculator;

import calculator.calculatorException.CalculatorException;
import calculator.commands.*;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 30.03.2017.
 */
public class CommandTest extends TestCase
{
	@Test
	public void testSumm() throws CalculatorException
	{
		Context context = new Context();
		context.pushToStack(15);
		context.pushToStack(28);
		Command cmd = new PlusCommand();
		try
		{
			cmd.execute(null,context,null);
		}
		catch (CalculatorException ex) {}
		Assert.assertEquals(43,context.popFromStack(),0);
	}

	@Test
	public void testDiv()
	{
		Context context = new Context();
		context.pushToStack(5);
		context.pushToStack(15);
		Command cmd = new DivCommand();
		try
		{
			cmd.execute(null,context,null);
		}
		catch (CalculatorException ex) {}
		Assert.assertEquals(3,context.popFromStack(),0);
	}

	@Test
	public void testMul()
	{
		Context context = new Context();
		context.pushToStack(5);
		context.pushToStack(15);
		Command cmd = new MulCommand();
		try
		{
			cmd.execute(null,context,null);
		}
		catch (CalculatorException ex) {}
		Assert.assertEquals(75,context.popFromStack(),0);
	}

	@Test
	public void testSub() {
		Context context = new Context();
		context.pushToStack(3);
		context.pushToStack(13);
		Command cmd = new SubCommand();
		try
		{
			context = cmd.execute(null, context, null);
		}
		catch (CalculatorException ex) {}
		Assert.assertEquals(10, context.peekAtStack(), 0);
	}

	@Test
	public void testSQRT()
	{
		Context context = new Context();
		context.pushToStack(49);
		Command cmd = new SqrtCommand();
		try
		{
			cmd.execute(null,context,null);
		}
		catch (CalculatorException ex) {}
		Assert.assertEquals(7,context.popFromStack(),0);
	}

	@Test
	public void testPush()
	{
		Context context = new Context();
		Command cmd = new PushCommand();
		List<String> arg = new ArrayList<String>();
		arg.add("45");
		try
		{
			cmd.execute(arg,context,null);
		}
		catch (CalculatorException ex) {}
		Assert.assertEquals(45,context.popFromStack(),0);
	}

	@Test
	public void testPop()
	{
		Context context = new Context();
		Command cmd = new PopCommand();
		context.pushToStack(45);
		context.pushToStack(56);
		try
		{
			cmd.execute(null,context,null);
		}
		catch (CalculatorException ex) {}
		Assert.assertEquals(45,context.popFromStack(),0);
	}

	@Test
	public void testPrint()
	{
		Context context = new Context();
		context.pushToStack(4.0);
		Command cmd = new PrintCommand();
		OutputStream out = new ByteArrayOutputStream();
		PrintStream str = new PrintStream(out);
		try
		{
			cmd.execute(null, context, str);
		}
		catch (CalculatorException ex){}
		OutputStream out2 = new ByteArrayOutputStream();
		str = new PrintStream(out2);
		str.println("4.0");
		Assert.assertEquals(out2.toString(), out.toString());
	}
	public void testDefine()
	{
		Context context = new Context();
		Command cmd = new DefineCommand();
		List<String> args = new ArrayList<String>();
		args.add("Any");
		args.add("45");
		try
		{
			cmd.execute(args,context,null);
		}
		catch (CalculatorException ex) {}
		Assert.assertEquals(45, context.getConstant("Any"),0);
	}
}