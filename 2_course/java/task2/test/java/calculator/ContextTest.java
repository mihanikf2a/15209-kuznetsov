package calculator;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

import java.util.EmptyStackException;
import java.util.NoSuchElementException;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 30.03.2017.
 */
public class ContextTest extends TestCase
{
	@Test(expected = EmptyStackException.class)
	public void testContextPushPop()
	{
		Context context = new Context();
		context.pushToStack(4);
		context.pushToStack(214.123);
		Assert.assertEquals(214.123, context.popFromStack(), 0);
		Assert.assertEquals(4, context.popFromStack(), 0);
		context.popFromStack();
	}

	@Test
	public void testContextPeek()
	{
		Context context = new Context();
		context.pushToStack(4);
		context.pushToStack(214.123);
		Assert.assertEquals(214.123, context.peekAtStack(), 0);
		Assert.assertEquals(214.123, context.popFromStack(), 0);
	}
	@Test
	public void testContextDefs()
	{
		Context context = new Context();
		context.setConstant("Any",22.54);
		context.setConstant("More", 999.);
		context.setConstant("x",141.);
		Assert.assertEquals(22.54, context.getConstant("Any"), 0);
		Assert.assertEquals(141., context.getConstant("x"), 0);
		Assert.assertEquals(999., context.getConstant("More"), 0);
		Assert.assertEquals(22.54, context.getConstant("Any"), 0);
	}

	@Test(expected = NoSuchElementException.class)
	public void testContextDefs2()
	{
		Context context = new Context();
		context.setConstant("Any", 22.54);
		Assert.assertEquals(22.54, context.getConstant("Other"), 0);
	}
}