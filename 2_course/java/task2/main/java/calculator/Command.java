package calculator;

import calculator.calculatorException.CalculatorException;

import java.io.PrintStream;
import java.util.List;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 03.03.2017.
 */

public interface Command
{
    public Context execute(List<String> commandLine,Context context,PrintStream outStream) throws CalculatorException;
}
