package calculator;

import calculator.calculatorException.CalculatorFactoryException;
import calculator.calculatorException.CalculatorFactoryNoSuchElementsException;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import calculator.calculatorException.CalculatorException;

import java.io.*;
import java.util.*;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 03.03.2017.
 */
public class Calculator
{
    private static final Logger logger = LogManager.getLogger(Calculator.class);
    private Context context = new Context();
    private List<String> parseCommand(String commandLine)
    {
        if(commandLine.contains(" "))
        {
            List<String> rezultList = new ArrayList<String>();
            int i=0,j=0;
            while ((i = commandLine.indexOf(" ",i))>0)
            {
                rezultList.add(commandLine.substring(j,i));
                j = ++i;
            }
            rezultList.add(commandLine.substring(j,commandLine.length()));
            return rezultList;
        }
        else
        {
            if(commandLine!=null)
            {
                List<String> rezultList = new ArrayList<String>();
                rezultList.add(commandLine);
                return rezultList;
            }
        }
        return null;
    }
    public void run(InputStream stream, OutputStream outputStream)
    {
        logger.info("\n===============================" +
                "\nNew start of program at " + java.util.Calendar.getInstance().getTime().toString());

        BufferedReader bufferedStream = new BufferedReader(new InputStreamReader(stream));
        PrintStream output = new PrintStream(outputStream);

        String line;
        try
        {
            while ((line = bufferedStream.readLine())!=null)
            {
                if(line.length()==0)
                {
                    logger.info("End of stream was found.");
                    return;
                }
                if ('#' == line.charAt(0))
                {
                    logger.info("Comment line" + line + " was found");
                    continue;
                }
                logger.info("Was read line: " + line);
                List<String> list = parseCommand(line);
                if(list != null)
                {
                    String commandName = list.get(0);
                    Command command = null;
                    try
                    {
                        command = CommandFactory.getInstance().getCommand(commandName);
                        list.remove(0);
                        context = command.execute(list, context, output);
                        logger.trace("Command has executed: " + commandName+"");
                    }
                    catch (CalculatorException ex)
                    {
                        logger.error(ex.getLocalizedMessage());
                        ex.printStackTrace();
                        continue;
                    }
                    catch (CalculatorFactoryNoSuchElementsException ex)
                    {
                        logger.error("NoSuchElementsException: "+ ex.getLocalizedMessage());
                        ex.printStackTrace();
                        continue;
                    }
                    catch (IOException ex)
                    {
                        logger.error("IOException: " + ex.getLocalizedMessage());
                        ex.printStackTrace();
                    }
                    catch (CalculatorFactoryException ex)
                    {
                        logger.error("CalculatorFactoryException: " + ex.getLocalizedMessage());
                        ex.printStackTrace();
                    }
                }
                else
                {
                    return;
                }

            }
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }
}
