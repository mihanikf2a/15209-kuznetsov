package calculator.commands;

import calculator.Command;
import calculator.Context;
import calculator.calculatorException.CalculatorException;
import calculator.calculatorException.CalculatorNotEnoughtElements;
import calculator.calculatorException.CalculatorStackIsEmptyException;

import java.io.PrintStream;
import java.util.List;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 03.03.2017.
 */
public class PlusCommand implements Command
{
    public Context execute(List<String> commandLine, Context context,PrintStream outStream) throws CalculatorException
    {
        if(context.stackIsEmpty())
        {
            throw new CalculatorStackIsEmptyException();
        }
        if (context.stackSize()<2)
        {
            throw new CalculatorNotEnoughtElements();
        }
        double arg1 = context.popFromStack();
        double arg2 = context.popFromStack();
        context.pushToStack(arg1+arg2);
        return context;
    }
}
