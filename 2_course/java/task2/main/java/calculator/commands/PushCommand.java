package calculator.commands;

import calculator.Command;
import calculator.Context;

import java.io.PrintStream;
import java.util.List;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 03.03.2017.
 */
public class PushCommand implements Command
{
    public Context execute(List<String> commandLine, Context context,PrintStream outStream)
    {
        double value;
        try
        {
            value = Double.parseDouble(commandLine.get(0));
        }
        catch (NumberFormatException ex)
        {
            value = context.getConstant(commandLine.get(0));
        }
        context.pushToStack(value);
        return context;
    }
}
