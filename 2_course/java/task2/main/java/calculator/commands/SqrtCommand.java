package calculator.commands;

import calculator.Command;
import calculator.Context;
import calculator.calculatorException.CalculatorException;
import calculator.calculatorException.CalculatorStackIsEmptyException;

import java.io.PrintStream;
import java.util.List;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 03.03.2017.
 */
public class SqrtCommand implements Command
{
    public Context execute(List<String> commandLine, Context context,PrintStream outStream) throws CalculatorException
    {
        if(context.stackIsEmpty())
        {
            throw new CalculatorStackIsEmptyException();
        }
        double arg = context.popFromStack();
        context.pushToStack(Math.sqrt(arg));
        return context;
    }
}
