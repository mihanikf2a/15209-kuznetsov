package calculator.commands;

import calculator.Command;
import calculator.Context;
import calculator.calculatorException.CalculatorException;
import calculator.calculatorException.CalculatorIlligalArgumentException;

import java.io.PrintStream;
import java.util.List;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 03.03.2017.
 */
public class DefineCommand implements Command
{
    public Context execute(List<String> commandLine, Context context,PrintStream outStream) throws CalculatorException
    {
        if (commandLine.size() < 2)
        {
            throw new CalculatorIlligalArgumentException("DefineCommand: Input dictionary is not full.");
        }
        String arg1 = commandLine.get(0);
        double arg2;
        try
        {
            double testArg1 = Double.parseDouble(commandLine.get(0));
            throw new CalculatorIlligalArgumentException("DefineCommand: First argument can't be a value!");
        }
        catch (NumberFormatException e){}
        try
        {
            arg2 = Double.parseDouble(commandLine.get(1));
        }
        catch (NumberFormatException e)
        {
            arg2 = context.getConstant(commandLine.get(1));
        }
        context.setConstant(arg1, arg2);
        return context;
    }
}