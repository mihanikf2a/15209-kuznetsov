package calculator.calculatorException;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 24.03.2017.
 */
public class CalculatorDivisionByZeroException extends CalculatorException
{
    public CalculatorDivisionByZeroException()
    {
        super("Division by zero");
    }
}
