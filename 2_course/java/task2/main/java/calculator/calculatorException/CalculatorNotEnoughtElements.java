package calculator.calculatorException;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 24.03.2017.
 */
public class CalculatorNotEnoughtElements extends CalculatorException
{
    public CalculatorNotEnoughtElements()
    {
        super("Not enought element in stack");
    }
}
