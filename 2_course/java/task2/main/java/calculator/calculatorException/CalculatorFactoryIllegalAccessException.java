package calculator.calculatorException;

/**
 * Created by mihlm on 28.03.2017.
 */
public class CalculatorFactoryIllegalAccessException extends CalculatorFactoryException
{
    public CalculatorFactoryIllegalAccessException()
    {
        super("Calculator Factory Illegal Access Exception");
    }
}
