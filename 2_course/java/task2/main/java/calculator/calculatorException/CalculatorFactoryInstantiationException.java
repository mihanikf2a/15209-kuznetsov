package calculator.calculatorException;

/**
 * Created by mihlm on 28.03.2017.
 */
public class CalculatorFactoryInstantiationException extends CalculatorFactoryException
{
    public CalculatorFactoryInstantiationException()
    {
        super("Calculator Factory Instantiation Exception");
    }
}
