package calculator.calculatorException;

/**
 * Created by Mike Kuznetsov m.kuznetsov@g.nsu.ru on 30.03.2017.
 */
public class CalculatorFactoryNoSuchElementsException extends CalculatorFactoryException
{
	public CalculatorFactoryNoSuchElementsException(String newEx)
	{
		super("NoSuchElementsException: " + newEx);
	}
}
