package calculator.calculatorException;

/**
 * Created by mihlm on 28.03.2017.
 */
public class CalculatorFactoryException extends Exception
{
    public CalculatorFactoryException(String newEx)
    {
        super(newEx);
    }
}
