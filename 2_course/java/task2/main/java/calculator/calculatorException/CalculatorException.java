package calculator.calculatorException;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 24.03.2017.
 */
public class CalculatorException extends Exception
{
    public CalculatorException (String newEx)
    {
        super(newEx);
    }
}
