package calculator.calculatorException;

/**
 * Created by mihlm on 28.03.2017.
 */
public class CalculatorFactoryClassNotFoundException extends CalculatorFactoryException
{
    public CalculatorFactoryClassNotFoundException()
    {
        super("ClassNotFoundException");
    }
}
