package calculator.calculatorException;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 24.03.2017.
 */
public class CalculatorStackIsEmptyException extends CalculatorException
{
    public CalculatorStackIsEmptyException()
    {
        super("Stack is epmty");
    }
}
