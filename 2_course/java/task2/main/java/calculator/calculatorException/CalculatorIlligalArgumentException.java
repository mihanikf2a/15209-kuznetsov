package calculator.calculatorException;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 24.03.2017.
 */
public class CalculatorIlligalArgumentException extends CalculatorException
{
    public CalculatorIlligalArgumentException(String newEx)
    {
        super(newEx);
    }
}
