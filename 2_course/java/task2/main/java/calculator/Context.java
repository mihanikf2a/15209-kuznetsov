package calculator;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Stack;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 03.03.2017.
 */
public class Context
{
    private Stack<Double> stack = new Stack<Double>();
    private HashMap<String, Double> constants = new HashMap<String, Double>();

    public double getConstant(String constName) throws NoSuchElementException
    {
        if (!constants.containsKey(constName))
        {
            throw new NoSuchElementException("Stack doesn't contain definition of " + constName);
        }
        return constants.get(constName);
    }

    public void setConstant(String constName, Double value)
    {
        constants.put(constName, value);
    }

    public void pushToStack(double value)
    {
        stack.push(value);
    }

    public double popFromStack() throws EmptyStackException
    {
        return stack.pop();
    }

    public double peekAtStack() throws EmptyStackException
    {
        return stack.peek();
    }
    public boolean stackIsEmpty()
    {
        return stack.empty();
    }
    public int stackSize()
    {
        return stack.size();
    }
}
