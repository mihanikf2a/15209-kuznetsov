package calculator;

import calculator.calculatorException.CalculatorFactoryClassNotFoundException;
import calculator.calculatorException.CalculatorFactoryIllegalAccessException;
import calculator.calculatorException.CalculatorFactoryInstantiationException;
import calculator.calculatorException.CalculatorFactoryNoSuchElementsException;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 03.03.2017.
 */
public class CommandFactory
{
    //private static HashMap<String, String> conformances = null;
    private static Properties properties = null;
    private static HashMap<String, Class<Command>> classes = null;
    private static CommandFactory cf = null;

    private CommandFactory() throws IOException
    {
        classes = new HashMap<String, Class<Command>>();
        InputStream conformancesAsStream = CommandFactory.class.getResourceAsStream("/CommandsFactory.properties");
        try
        {
            properties = new Properties();
            properties.load(conformancesAsStream);
        }
        finally
        {
            conformancesAsStream.close();
        }
    }

    public static CommandFactory getInstance() throws IOException
    {
        if(cf==null)
        {
            cf = new CommandFactory();
        }
        return cf;
    }

    public static Command getCommand(String commandName) throws CalculatorFactoryClassNotFoundException,CalculatorFactoryNoSuchElementsException,
            CalculatorFactoryIllegalAccessException, CalculatorFactoryInstantiationException
    {
        Command cmd = null;
        Class<?> cls = null;

        String key = properties.getProperty(commandName);
        if(key==null)
        {
            throw new CalculatorFactoryNoSuchElementsException("Invalid command request: " + commandName);
        }
        if (!classes.containsKey(key))
        {
            try {
                cls = Class.forName(key);
            }
            catch (ClassNotFoundException ex)
            {
                throw new CalculatorFactoryClassNotFoundException();
            }
            classes.put(commandName, (Class<Command>)cls);
        }
        else
        {
            cls = classes.get(commandName);
        }
        try
        {
            cmd = (Command) cls.newInstance();
        }
        catch (InstantiationException ex)
        {
            throw  new CalculatorFactoryInstantiationException();
        }
        catch (IllegalAccessException ex)
        {
            throw new CalculatorFactoryIllegalAccessException();
        }
        return cmd;
    }
}
