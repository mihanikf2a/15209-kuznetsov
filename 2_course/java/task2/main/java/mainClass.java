import calculator.Calculator;

import java.io.*;

/**
 * Created by Mike Kuznetsov, mihlmih@mail.ru on 03.03.2017.
 */
public class mainClass
{
    public static void main(String argv[])
    {
        Calculator calc = new Calculator();
        InputStream inputStream = null;
        OutputStream outputStream = null;
        if(argv.length>=1)
        {
            try
            {
                inputStream = new FileInputStream(argv[0]);
            }
            catch (FileNotFoundException ex)
            {
                System.err.println(ex.fillInStackTrace());
            }
        }
        else
        {
            inputStream = System.in;
        }
        if(argv.length==2)
        {
            try
            {
                outputStream = new FileOutputStream(argv[1]);
            }
            catch (FileNotFoundException ex)
            {
                System.err.println(ex.fillInStackTrace());
            }
        }
        else
        {
            outputStream = System.out;
        }
        calc.run(inputStream,outputStream);
        if(argv.length>=1)
        {
            try
            {
                inputStream.close();
            }
            catch (IOException ex)
            {
                System.err.println(ex.fillInStackTrace());
            }
        }
    }
}
