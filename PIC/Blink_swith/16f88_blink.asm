; compiler: jal 2.4o (compiled May  8 2011)

; command line:  C:\JALPack\compiler\jalv2.exe D:\Projects\15209-kuznetsov\pic\16f88_blink.jal -s C:\JALPack\lib -no-variable-reuse
                                list p=16f88, r=dec
                                errorlevel -306 ; no page boundary warnings
                                errorlevel -302 ; no bank 0 warnings
                                errorlevel -202 ; no 'argument out of range' warnings

                             __config 0x2007, 0x3f38
                             __config 0x2008, 0x3fff
datahi_set macro val
  bsf 3, 6 ; STATUS<rp1>
  endm
datahi_clr macro val
  bcf 3, 6 ; STATUS<rp1>
  endm
datalo_set macro val
  bsf 3, 5 ; STATUS<rp0>
  endm
datalo_clr macro val
  bcf 3, 5 ; STATUS<rp0>
  endm
irp_clr macro
  bcf 3, 7 ; STATUS<irp>
  endm
irp_set macro
  bsf 3, 7 ; STATUS<irp>
  endm
branchhi_set macro lbl
    bsf 10, 4 ; PCLATH<4>
  endm
branchhi_clr macro lbl
    bcf 10, 4 ; PCLATH<4>
  endm
branchlo_set macro lbl
    bsf 10, 3 ; PCLATH<3>
  endm
branchlo_clr macro lbl
    bcf 10, 3 ; PCLATH<3>
  endm
v_on                           EQU 1
v_off                          EQU 0
v_input                        EQU 1
v_output                       EQU 0
v__portb                       EQU 0x0006  ; _portb
v__portb_shadow                EQU 0x0022  ; _portb_shadow
v_pin_b4                       EQU 0x0006  ; pin_b4-->_portb:4
v_adcon0                       EQU 0x001f  ; adcon0
v_trisb                        EQU 0x0086  ; trisb
v_pin_b5_direction             EQU 0x0086  ; pin_b5_direction-->trisb:5
v_pin_b4_direction             EQU 0x0086  ; pin_b4_direction-->trisb:4
v_ansel                        EQU 0x009b  ; ansel
v_cmcon                        EQU 0x009c  ; cmcon
v_adcon1                       EQU 0x009f  ; adcon1
v__pic_temp                    EQU 0x0020  ; _pic_temp-->_pic_state
v__pic_state                   EQU 0x0020  ; _pic_state
v___x_54                       EQU 0x0022  ; x-->_portb_shadow:5
v___x_55                       EQU 0x0022  ; x-->_portb_shadow:5
v___x_56                       EQU 0x0022  ; x-->_portb_shadow:5
;   26 include 16f88                    -- target PICmicro
                               org      0
l__main
;   45 enable_digital_io()                -- make all pins digital I/O
; C:\JALPack\lib/16f88.jal
;  690    ANSEL  = 0b0000_0000       -- all digital
                               datalo_set v_ansel
                               clrf     v_ansel
; D:\Projects\15209-kuznetsov\pic\16f88_blink.jal
;   45 enable_digital_io()                -- make all pins digital I/O
; C:\JALPack\lib/16f88.jal
;  712    analog_off()
; D:\Projects\15209-kuznetsov\pic\16f88_blink.jal
;   45 enable_digital_io()                -- make all pins digital I/O
; C:\JALPack\lib/16f88.jal
;  697    ADCON0 = 0b0000_0000         -- disable ADC
                               datalo_clr v_adcon0
                               clrf     v_adcon0
;  698    ADCON1 = 0b0000_0000
                               datalo_set v_adcon1
                               clrf     v_adcon1
; D:\Projects\15209-kuznetsov\pic\16f88_blink.jal
;   45 enable_digital_io()                -- make all pins digital I/O
; C:\JALPack\lib/16f88.jal
;  713    adc_off()
; D:\Projects\15209-kuznetsov\pic\16f88_blink.jal
;   45 enable_digital_io()                -- make all pins digital I/O
; C:\JALPack\lib/16f88.jal
;  705    CMCON  = 0b0000_0111        -- disable comparator
                               movlw    7
                               movwf    v_cmcon
; D:\Projects\15209-kuznetsov\pic\16f88_blink.jal
;   45 enable_digital_io()                -- make all pins digital I/O
; C:\JALPack\lib/16f88.jal
;  714    comparator_off()
; D:\Projects\15209-kuznetsov\pic\16f88_blink.jal
;   45 enable_digital_io()                -- make all pins digital I/O
;   51 pin_B5_direction =  output
                               bcf      v_trisb, 5 ; pin_b5_direction
;   53 pin_B4_direction = input
                               bsf      v_trisb, 4 ; pin_b4_direction
;   54 led = off
                               datalo_clr v__portb_shadow ; x54
                               bcf      v__portb_shadow, 5 ; x54
; C:\JALPack\lib/16f88.jal
;  271    _PORTB = _PORTB_shadow
                               movf     v__portb_shadow,w
                               movwf    v__portb
; D:\Projects\15209-kuznetsov\pic\16f88_blink.jal
;   54 led = off
;   56 forever loop
l__l87
;   57    led = off
                               bcf      v__portb_shadow, 5 ; x55
; C:\JALPack\lib/16f88.jal
;  271    _PORTB = _PORTB_shadow
                               movf     v__portb_shadow,w
                               movwf    v__portb
; D:\Projects\15209-kuznetsov\pic\16f88_blink.jal
;   57    led = off
;   59    if button == 0 then
                               btfsc    v__portb, 4 ; pin_b4
                               goto     l__l87
;   60       led = on
                               bsf      v__portb_shadow, 5 ; x56
; C:\JALPack\lib/16f88.jal
;  271    _PORTB = _PORTB_shadow
                               movf     v__portb_shadow,w
                               movwf    v__portb
; D:\Projects\15209-kuznetsov\pic\16f88_blink.jal
;   60       led = on
;   61       _usec_delay(50000)
                               datalo_clr v__pic_temp
                               datahi_clr v__pic_temp
                               movlw    116
                               movwf    v__pic_temp
l__l95
                               movlw    85
                               movwf    v__pic_temp+1
l__l96
                               branchhi_clr l__l96
                               branchlo_clr l__l96
                               decfsz   v__pic_temp+1,f
                               goto     l__l96
                               branchhi_clr l__l95
                               branchlo_clr l__l95
                               decfsz   v__pic_temp,f
                               goto     l__l95
                               nop      
;   62    end if
;   63 end loop
                               goto     l__l87
                               end
